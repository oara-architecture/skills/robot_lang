skillset test {
    resource test_initial {
        initial state_initial
    }

    resource test_named {
        initial state_initial
        a -> b
        a -> b
        c -> a
    }

    resource test_extern {
        initial state_initial
        extern a -> b
        extern a -> b
    }
}