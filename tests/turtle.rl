type {
    float
    Position
    bool
}

skillset turtle {

    data position: Position

    function at(p: Position): bool
    function can_dive(p: Position): bool

    resource turtle_status {
        initial ON_SURFACE
        ON_SURFACE -> IN_WATER
        IN_WATER -> ON_SURFACE
    }

    resource sensor {
        initial OFF
        OFF -> ON
        ON -> OFF
        extern ON -> OUT_OF_SERVICE
        extern OFF -> OUT_OF_SERVICE
    }

    resource autonomous_control {
        initial ON
        extern ON -> OFF
        extern OFF -> ON
    }

    resource control_mode {
        initial IDLE
        IDLE -> MOVING
        MOVING -> IDLE
    }

    skill dive {
        progress=1.0

        effect {
            in_water: turtle_status->IN_WATER control_mode->IDLE
            moving: control_mode->MOVING
            idle: control_mode->IDLE
        }

        precondition {
            depth_ok: at(position) && can_dive(position)
            on_surface_ready: resource=((turtle_status == ON_SURFACE) && (control_mode == IDLE))
            autonomous: resource=(autonomous_control == ON)
            success moving
        }

        invariant autonomous: resource=(autonomous_control == ON) violation=idle

        result {
            SUCCESS: apply=in_water
            FAILURE: apply=idle
        }
    }

    skill ascend {
        progress=1.0

        effect {
            on_surface: turtle_status->ON_SURFACE control_mode->IDLE
            moving: control_mode->MOVING
            idle: control_mode->IDLE
        }

        precondition {
            in_water_ready: resource=((turtle_status == IN_WATER) && (control_mode == IDLE))
            autonomous: resource=(autonomous_control == ON)
            success moving
        }

        invariant autonomous: resource=(autonomous_control == ON) violation=idle

        result {
            SUCCESS: apply=on_surface
            FAILURE: apply=idle
        }
    }

    skill move_on_surface {
        progress=1.0

        input target: Position

        effect {
            moving: control_mode->MOVING
            idle: control_mode->IDLE
        }

        precondition {
            on_surface: resource=((turtle_status == ON_SURFACE) && (control_mode == IDLE))
            current_position: at(position)
            autonomous: resource=(autonomous_control == ON)
            success moving
        }

        invariant {
            on_surface: resource=(robot_status == ON_SURFACE) violation=idle
            autonomous: resource=(autonomous_control == ON) violation=idle
        }

        result {
            ARRIVED: ((! at(position)) && at(target)) apply=idle
            ABORTED: apply=idle
        }
    }

    skill move {
        progress=1.0

        input target: Position

        effect {
            moving: control_mode->MOVING
            idle: control_mode->IDLE
        }

        precondition {
            in_water: resource=((turtle_status == IN_WATER) && (control_mode == IDLE))
            current_position: at(position)
            autonomous: resource=(autonomous_control == ON)
            success moving
        }

        invariant {
            in_water: resource=(robot_status == IN_WATER) violation=idle
            autonomous: resource=(autonomous_control == ON) violation=idle
        }

        result {
            ARRIVED: ((! at(position)) && at(target)) apply=idle
            ABORTED: apply=idle
        }
    }

    skill turn_sensor_on {
        progress=1.0
        effect sensor_on: sensor->ON
        precondition sensor_off: resource=(sensor == OFF)
        result ON: apply=sensor_on
    }

    skill turn_sensor_off {
        progress=1.0
        effect sensor_off: sensor->OFF
        precondition sensor_on: resource=(sensor == ON)
        result OFF: apply=sensor_off
    }

    skill search {
        progress=1.0

        input timeout: float

        effect {
            moving: control_mode->MOVING
            idle: control_mode->IDLE
        }

        precondition {
            in_water: resource=((turtle_status == IN_WATER) && (control_mode == IDLE))
            sensor_on: resource=(sensor == ON)
            autonomous: resource=(autonomous_control == ON)
            success moving
        } 

        invariant {
            autonomous: resource=(autonomous_control == ON) violation=idle
        }

        result {
            FOUND: apply=idle moving
            TIMEDOUT: apply=idle moving
        }
    }

}