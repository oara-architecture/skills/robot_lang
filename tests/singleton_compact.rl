skillset XXX {

    skill Y {

        progress = 1

        input i: T

        effect e: R1->S1 R2->S2

        precondition p: true resource=R1==S1 success=e

        invariant i: true resource=R2==S2 violation=e

        mode m: true duration=1 post=(R1==S1) apply=e
    }

}