skillset test {

    skill test_use_state {
        progress=0
        precondition {
            p: resource=(test_resource == state_1)
        }
    }

    skill test_use_not {
        progress=0
        precondition {
            p: resource=(test_resource != state_1)
        }
    }

    skill test_use_or {
        progress=0
        precondition {
            p: resource=((test_resource == state_1) || (test_resource == state_2) || (test_resource == state_3))
        }
    }

}
