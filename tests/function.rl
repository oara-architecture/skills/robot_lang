skillset test {

    function f_void()

    function f_noparam(): A

    function f_noreturn(a: A, b: B)

    function f_full(a: A, b: B): C

    function {
        f_block1()
        f_block2(a: A): B
    }

}