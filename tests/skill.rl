skillset test {

    skill test_progress_int {
        progress=1
    }

    skill test_progress_float {
        progress=1.1
    }

    skill test_input {
        progress=0
        input {
            a: A
            b: B
        }
    }

    skill test_pre {
        progress=0
        precondition {
            p: a > 0
        }
    }

    skill test_use {
        progress=0
    }

    skill test_use_pre {
        progress=0
        precondition {
            test_resource {
                resource test_resource==state_1
            }
        }
    }

    skill test_use_start {
        progress=0

        effect {
            eff {
                test_resource -> state_1
            }
        }

        precondition {
            success eff
        }
    }

    skill test_use_inv {
        progress=0
        invariant i: test_resource==state_1
    }

}