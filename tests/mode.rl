skillset test {

    skill test_mode_empty {
        progress=1
        mode {
            m1 {}
            m2 {}
        }
    }

    skill test_mode_duration {
        progress=1
        mode {
            m1 {
                duration 1.0
            }
            m2 {
                duration inf
            }
        }
    }

    skill test_mode_post {
        progress=1
        mode {
            m1 {
                (a == 1)
            }
            m2 {
                (a == 1) && (b > 10)
            }
        }
    }

    skill test_mode_resource {
        progress=1

        effect {
            eff: test_resource -> state1
        }

        mode {
            m_empty {
                test_resource
            }
            m_post {
                post (test_resource == state1) ||  (test_resource == state2)
            }
            m_effect {
                apply eff
            }
        }
    }
}