from .lock import Lock
from .block import Block
from .info import Info

class Input(Lock):
    """A skill input.
    
    :param info: parsing information.
    :param name: input name.
    :param itype: input type name.
    """

    def __init__(self, info: Info, name: str, itype: str):
        self.__info = info
        self.__name = name
        self.__type = itype
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get input name."""
        return self.__name

    @property
    def type(self) -> str:
        """Get input type."""
        return self.__type

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Input(' + repr(self.name) + ', ' + repr(self.type) + ', ' + str(self.info) + ')'

    def pretty(self):
        return str(self.name) + ': ' + str(self.type)

    def json(self):
        s  = "\t\t\t\t\t\t{\n"
        s += "\t\t\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        s += "\t\t\t\t\t\t\t\"type\": \"" + str(self.type) + "\"\n"
        s += "\t\t\t\t\t\t}\n"
        return s


class InputBlock(Block):
    """A block of inputs."""

    def __str__(self):
        return 'InputBlock'

    def pretty(self):
        if self.list == []:
            return ''
        s = '      input {\n'
        for i in self.list:
            s += '        ' + i.pretty() + '\n'
        s += '      }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\t\t\"inputs\": [\n"
            s += self.list[0].json()
            for d in self.list[1:]:
                s += "\t\t\t\t\t\t,\n"
                s += d.json()
            s += "\t\t\t\t\t],\n"
        else:
            s += "\t\t\t\t\t\"inputs\": [],\n"
        return s
