from .lock import Lock
from .block import Block
from .info import Info

class Type:
    """A type.
    
    :param info: parsing information.
    :param name: type name.
    """

    __slots__ = ['__name', '__info']

    def __init__(self, info: Info, name: str):
        self.__info = info
        self.__name = name

    @property
    def name(self) -> str: 
        """Get type name."""
        return self.__name

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    def __str__(self): return str(self.name)

    def __repr__(self): return 'Type(' + repr(self.name) + ', ' + str(self.info) + ')'

    def json(self):
        s = "\t\t{\n"
        s += "\t\t\t\"name\": \"" + str(self.name) + "\"\n"
        s += "\t\t}\n"
        return s

class TypeBlock(Block):
    """Block of types."""

    def __str__(self): 
        return 'TypeBlock'

    def extend(self, typeBlock):
        self._unlock()
        self.list.extend(typeBlock.list)
        for t in typeBlock.list:
            setattr(self, t.name, t)
        self._lock()

    def pretty(self):
        s = 'type {\n'
        for t in self.list:
            s += '  ' + str(t.name) + '\n'
        s += '}\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\"types\": [\n"
            s += self.list[0].json()
            for t in self.list[1:]:
                s += "\t\t,\n"
                s += t.json()
            s += "\t],\n"
        else:
            s += "\t\"types\": [],\n"
        return s
