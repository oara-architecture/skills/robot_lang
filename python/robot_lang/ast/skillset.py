from .lock import Lock
from .block import Block
from .info import Info
from .data import DataBlock
from .function import FunctionBlock
from .skill import SkillBlock
from .resource_mod import ResourceBlock

class SkillSet(Lock):
    """A skillset.
    
    :param info: parsing information.
    :param name: skillset name.
    :param data_block: skillset data.
    :param fun_block: skillset functions.
    :param res_block: skillset resources.
    :param skill_block: skillset skills.
    """

    def __init__(self, info: Info, name: str, data_block: DataBlock, 
                 fun_block: FunctionBlock, res_block: ResourceBlock, skill_block: SkillBlock):
        self.__info = info
        self.__name = name
        self.__data = data_block
        self.__functions = fun_block
        self.__resources = res_block
        self.__skills = skill_block
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing info."""
        return self.__info

    @property
    def name(self) -> str:
        """Get skillset name."""
        return self.__name

    @property
    def data(self) -> DataBlock:
        """Get skillset data."""
        return self.__data

    @property
    def functions(self) -> FunctionBlock:
        """Get skillset functions."""
        return self.__functions

    @property
    def resources(self) -> ResourceBlock:
        """Get skillset resources."""
        return self.__resources

    @property
    def skills(self) -> SkillBlock:
        """Get skillset skills."""
        return self.__skills

    def __str__(self): return str(self.name)

    def __repr__(self): return 'SkillSet(' + repr(self.name) + ', ' + str(self.info) + ')'

    def pretty(self):
        s = 'skillset ' + str(self.name) + ' {\n'
        s += self.data.pretty()
        s += self.functions.pretty()
        s += self.resources.pretty()
        s += self.skills.pretty()
        s += '}\n'
        return s

    def json(self):
        s = "\t\t{\n"
        s += "\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        s += self.data.json()
        s += self.functions.json()
        s += self.resources.json()
        s += self.skills.json()
        s += "\t\t}\n"
        return s


class SkillSetBlock(Block):
    """Block of skillsets."""

    def __str__(self): 
        return 'SkillSetBlock'

    def extend(self, skillSetBlock):
        self._unlock()
        self.list.extend(skillSetBlock.list)
        for r in skillSetBlock.list:
            setattr(self, r.name, r)
        self._lock()

    def pretty(self):
        s = ''
        for r in self.list:
            s += r.pretty()
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\"skillsets\": [\n"
            s += self.list[0].json()
            for t in self.list[1:]:
                s += "\t\t,\n"
                s += t.json()
            s += "\t]\n"
        else:
            s += "\t\"skillsets\": []\n"
        return s
