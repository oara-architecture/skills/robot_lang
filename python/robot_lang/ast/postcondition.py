from .lock import Lock
from .block import Block

class Postcondition(Lock):

    def __init__(self, info, expression):
        self.info = info
        self.expression = expression
        self._lock()

    def __str__(self):
        return str(self.expression)

    def __repr__(self):
        return 'Postcondition(' + str(self.info) + ')'

    def pretty(self):
        return '            ' + str(self.expression)

class PostconditionBlock(Block):

    def __str__(self):
        return 'PostconditionBlock'

    def pretty(self):
        if self.list == []:
            return ''
        s = '          postcondition {\n'
        for p in self.list:
            s += p.pretty() + '\n'
        s += '          }\n'
        return s
