from typing import List

from .lock import Lock
from .block import Block
from .info import Info

class Parameter(Lock):
    """A function parameter.
    
    :param info: parsing information.
    :param name: parameter name.
    :param ptype: parameter type name.
    """

    def __init__(self, info: Info, name: str, ptype: str):
        self.__info = info
        self.__name = name
        self.__type = ptype
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get parameter name."""
        return self.__name

    @property
    def type(self) -> str:
        """Get parameter type."""
        return self.__type

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Parameter(' + repr(self.name) + ', ' + repr(self.type) + ', ' + str(self.info) + ')'

    def pretty(self):
        return str(self.name) + ': ' + str(self.type)

    def json(self):
        s  = "\t\t\t\t\t\t{\n"
        s += "\t\t\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        s += "\t\t\t\t\t\t\t\"type\": \"" + str(self.type) + "\"\n"
        s += "\t\t\t\t\t\t}\n"
        return s


class Function(Lock):
    """A function.
    
    :param info: parsing information.
    :param name: function name.
    :param parameters: function parameters.
    :param ftype: function return type name.
    """

    def __init__(self, info: Info, name: str, parameters: List[Parameter], ftype: str):
        self.__info = info
        self.__name = name
        self.__parameters = parameters
        self.__type = ftype
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get function name."""
        return self.__name

    @property
    def type(self) -> str:
        """Get function return type."""
        return self.__type

    @property
    def parameters(self) -> List[Parameter]:
        """Get function parameters."""
        return self.__parameters

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Function(' + repr(self.name) + ', ' + repr(self.parameters) + ', ' + repr(self.type) + ', ' + str(self.info) + ')'

    def pretty(self):
        s = str(self.name) + '('
        if self.parameters != []:
            head, *tail = self.parameters
            s += head.pretty()
            for p in tail:
                s += ', ' + p.pretty()
        s += '): '
        s += str(self.type)
        return s

    def json(self):
        s  = "\t\t\t\t{\n"
        s += "\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        s += "\t\t\t\t\t\"type\": \"" + str(self.type) + "\",\n"
        if self.parameters != []:
            s += "\t\t\t\t\t\"parameters\": [\n"
            s += self.parameters[0].json()
            for p in self.parameters[1:]:
                s += "\t\t\t\t\t\t,\n"
                s += p.json()
            s += "\t\t\t\t\t]\n"
        else:
            s += "\t\t\t\t\t\"parameters\": []\n"
        s += "\t\t\t\t}\n"
        return s


class FunctionBlock(Block):
    """A block of functions."""

    def __str__(self):
        return 'FunctionBlock'

    def pretty(self):
        s = '  function {\n'
        for f in self.list:
            s += '    ' + f.pretty() + '\n'
        s += '  }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\"functions\": [\n"
            s += self.list[0].json()
            for d in self.list[1:]:
                s += "\t\t\t\t,\n"
                s += d.json()
            s += "\t\t\t],\n"
        else:
            s += "\t\t\t\"functions\": [],\n"
        return s
