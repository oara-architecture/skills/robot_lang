from typing import Any, List

from .lock import Lock
from .block import Block
from .info import Info
from .expression import Expression
from .resource_expression import ResourceExpression


class Precondition(Lock):
    """A precondition.
    
    :param info: parsing information.
    :param name: precondition name.
    :param expression: expression of the logical formula attached to this precondition.
    :param resource: expression of the resource formula attached to the precondition.
    :param failure: name of the effect in case of precondition failure.
    """

    def __init__(self, info: Info, name: str, 
                 expression: Expression, 
                 resource: ResourceExpression, 
                 failure: str):
        self.__info = info
        self.__name = name
        self.__expression = expression
        self.__resource = resource
        self.__failure = failure
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get precondition label."""
        return self.__name

    @property
    def expression(self) -> Expression:
        """Get logic expression of the precondition."""
        return self.__expression

    @property
    def resource(self) -> ResourceExpression:
        """Get resource expression of the precondition."""
        return self.__resource

    @property
    def failure(self):
        return self.__failure

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Precondition(' + str(self.info) + ')'

    def pretty(self):
        s = '        ' + str(self.name) + ' {\n'
        if self.expression != None:
            s += '          ' + str(self.expression) + '\n'
        if self.resource != None:
            s += '          resource ' + str(self.resource) + '\n'
        if self.failure != None:
            s += '          failure  ' + str(self.failure) + '\n'
        s += '        }\n'
        return s

    def json(self):
        s  = "\t\t\t\t\t\t{\n"
        s += "\t\t\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        if self.expression:
            s += "\t\t\t\t\t\t\t\"expression\": " + self.expression.json()
            if self.resource or self.failure:
                s += ",\n"
            else:
                s += "\n"
        if self.resource:
            s += "\t\t\t\t\t\t\t\"resource\": " + self.resource.json()
            if self.failure:
                s += ",\n"
            else:
                s += "\n"
        if self.failure:
            s += "\t\t\t\t\t\t\t\"failure\": \"" + str(self.failure) + "\"\n"
        s += "\t\t\t\t\t\t}\n"
        return s


class PreconditionBlock(Block):
    """A block of preconditions.
    
    :param preconditions: set of preconditions in this block.
    :param success: name of the effect to apply in case all the preconditions succeeded.
    """

    def __init__(self, preconditions: List[Precondition], success: str):
        self.__success = success
        Block.__init__(self, preconditions)

    @property
    def success(self) -> str:
        """Get effect to apply in case of success."""
        return self.__success

    def __str__(self):
        return 'PreconditionBlock'

    def pretty(self):
        if self.list == []:
            return ''
        s = '      precondition {\n'
        for p in self.list:
            s += p.pretty()
        if self.success != None:
            s += '        success ' + str(self.success) + '\n'
        s += '      }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\t\t\"preconditions\": [\n"
            s += self.list[0].json()
            for p in self.list[1:]:
                s += "\t\t\t\t\t\t,\n"
                s += p.json()
            s += "\t\t\t\t\t],\n"
        else:
            s += "\t\t\t\t\t\"preconditions\": [],\n"
        if self.success:
            s += "\t\t\t\t\t\"preconditions_success\": \"" + str(self.success) + "\",\n"
        return s
