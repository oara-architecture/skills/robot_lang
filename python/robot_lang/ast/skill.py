from .lock import Lock
from .block import Block
from .info import Info
from .input import InputBlock
from .effect import EffectBlock
from .precondition import PreconditionBlock
from .invariant import InvariantBlock
from .result import ResultBlock


class Skill(Lock):
    """A skill."""

    def __init__(self, info: Info, name: str, progress: float,
                 input_block: InputBlock, effect_block: EffectBlock,
                 precondition_block: PreconditionBlock,
                 invariant_block: InvariantBlock,
                 # interrupt_effect: E,
                 result_block: ResultBlock):
        self.__info = info
        self.__name = name
        self.__progress = progress
        self.__inputs = input_block
        self.__effects = effect_block
        self.__preconditions = precondition_block
        self.__invariants = invariant_block
        self.__interrupt_effect = None  # interrupt_effect
        self.__results = result_block
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get skill name."""
        return self.__name

    @property
    def progress(self) -> float:
        """Get skill progress period."""
        return self.__progress

    @property
    def inputs(self) -> InputBlock:
        """Get skill inputs."""
        return self.__inputs

    @property
    def effects(self) -> EffectBlock:
        """Get skill effects."""
        return self.__effects

    @property
    def preconditions(self) -> PreconditionBlock:
        """Get skill preconditions."""
        return self.__preconditions

    @property
    def invariants(self) -> InvariantBlock:
        """Get skill invariants."""
        return self.__invariants

    @property
    def interrupt_effect(self):
        return self.__interrupt_effect

    @property
    def results(self) -> ResultBlock:
        """Get skill results."""
        return self.__results

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Skill(' + repr(self.name) + ', ' + str(self.info) + ')'

    def __eq__(self, other):
        return str(self) == str(other)

    def __ge__(self, other):
        return str(self) >= str(other)

    def __gt__(self, other):
        return str(self) > str(other)

    def __le__(self, other):
        return str(self) <= str(other)

    def __lt__(self, other):
        return str(self) < str(other)

    def pretty(self):
        s = '    ' + str(self.name) + ' {\n'
        s += '      progress = ' + str(self.progress) + '\n'
        s += self.inputs.pretty()
        s += self.effects.pretty()
        s += self.preconditions.pretty()
        s += self.invariants.pretty()
        if self.interrupt_effect != None:
            s += '      interruption effect ' + \
                str(self.interrupt_effect) + '\n'
        s += self.results.pretty()
        s += '    }\n'
        return s

    def json(self):
        s = "\t\t\t\t{\n"
        s += "\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        s += "\t\t\t\t\t\"progress\": " + str(self.progress) + ",\n"
        s += self.inputs.json()
        s += self.effects.json()
        s += self.preconditions.json()
        s += self.invariants.json()
        if self.interrupt_effect:
            s += "\t\t\t\t\t\"interrupt_effect\": \"" + \
                str(self.interrupt_effect) + "\",\n"
        s += self.results.json()
        s += "\t\t\t\t}\n"
        return s


class SkillBlock(Block):
    """A block of skills."""

    def __str__(self):
        return 'SkillBlock'

    def pretty(self):
        s = '  skill {\n'
        for d in self.list:
            s += d.pretty()
        s += '  }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\"skills\": [\n"
            s += self.list[0].json()
            for d in self.list[1:]:
                s += "\t\t\t\t,\n"
                s += d.json()
            s += "\t\t\t]\n"
        else:
            s += "\t\t\t\"skills\": []\n"
        return s
