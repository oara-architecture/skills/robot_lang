from .lock import Lock
from .type import TypeBlock
from .skillset import SkillSetBlock

class Model(Lock):
    """A Robot Skills Model.

    :param typeBlock: block of Types
    :param skillSetBlock: block of SkillSets
    """

    def __init__(self, typeBlock: TypeBlock = TypeBlock(), skillSetBlock: SkillSetBlock = SkillSetBlock()):
        self.__types = typeBlock
        self.__skillsets = skillSetBlock
        # self._lock() # ???

    @property
    def types(self) -> TypeBlock:
        """Get model types."""
        return self.__types

    @property
    def skillsets(self) -> SkillSetBlock:
        """Get model skillsets."""
        return self.__skillsets

    def __str__(self): return 'model'

    def __repr__(self): return 'Model(types=' + str(self.types) + ', skillsets=' + str(self.skillsets) + ')'

    def extend(self, model):
        self._unlock()
        self.types.extend(model.types)
        self.skillsets.extend(model.skillsets)
        self._lock()


    def pretty(self):
        s = ""
        s += self.types.pretty()
        s += self.skillsets.pretty()
        return s

    def json(self):
        s = "{\n"
        s += self.types.json()
        s += self.skillsets.json()
        s += "}\n"
        return s
