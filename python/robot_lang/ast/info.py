class Info:
    """Parsing information.
    
    :param file: parsed file name.
    :param line: parsing line number.
    :param column: parsing column number.
    """

    __slots__ = ['__file', '__line', '__column']

    def __init__(self, file: str, line: int, column: int):
        self.__file = file
        self.__line = line
        self.__column = column

    @property
    def file(self) -> str:
        """File name.""" 
        return self.__file

    @property
    def line(self) -> int: 
        """Line number."""
        return self.__line

    @property
    def column(self) -> int: 
        """Column number."""
        return self.__column

    def __str__(self): return str(self.file) + ':' + str(self.line) + ':' + str(self.column)

    def __repr__(self): return 'Info(' + str(self.file) + ', ' + str(self.line) + ', ' + str(self.column) + ')'
