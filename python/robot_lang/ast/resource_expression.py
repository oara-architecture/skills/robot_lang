from typing import Union
from abc import ABC

from .lock import Lock
from .info import Info

class ResourceExpression(Lock, ABC):
    pass


class BinCompRes(ResourceExpression):
    """Binary comparator resource expression. Compares a resource with a state.
    
    :param info: parsing information.
    :param op: comparison operator ('==' or '!=').
    :param resource: resource name.
    :param state: resource state.
    """
    def __init__(self, info: Info, op: str, resource: str, state: str):
        self.__info = info
        self.__op = op
        self.__resource = resource
        self.__state = state
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def op(self) -> str:
        """Get operator."""
        return self.__op

    @property
    def resource(self) -> str:
        """Get compared resource."""
        return self.__resource

    @property
    def state(self) -> str:
        """Get comparing state."""
        return self.__state

    def __str__(self):
        return '(' + str(self.resource) + ' ' + str(self.op) + ' ' + str(self.state) + ')'

    def __repr__(self):
        return 'BinCompRes(' + repr(self.op) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \""
        if self.op == "==": s += "ResourceStateEq"
        if self.op == "!=": s += "ResourceStateNe"
        s += "\", "
        s += "\"resource\": \"" + str(self.resource) + "\", "
        s += "\"state\": \"" + str(self.state) + "\""
        s += " }"
        return s


class BinBoolRes(ResourceExpression):
    """Binary boolean resource expression.
    
    :param info: parsing information.
    :param op: expression operator ('&&', '||', '=>').
    :param left: left resource expression of the boolean expresion.
    :param right: right resource expression of the boolean expresion.
    """
    def __init__(self, info: Info, op: str, left: ResourceExpression, right: ResourceExpression):
        self.__info = info
        self.__op = op
        self.__left = left
        self.__right = right
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def op(self) -> str:
        """Get operator."""
        return self.__op

    @property
    def left(self) -> Union['BinBoolRes', BinCompRes]:
        """Get left expression."""
        return self.__left

    @property
    def right(self) -> Union['BinBoolRes', BinCompRes]:
        """Get right expression."""
        return self.__right

    def __str__(self):
        return '(' + str(self.left) + ' ' + str(self.op) + ' ' + str(self.right) + ')'

    def __repr__(self):
        return 'BinBoolRes(' + repr(self.op) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \""
        if self.op == "&&": s += "And"
        if self.op == "||": s += "Or"
        if self.op == "=>": s += "Implies"
        s += "\", "
        s += "\"left\": " + self.left.json() + ", "
        s += "\"right\": " + self.right.json()
        s += " }"
        return s

