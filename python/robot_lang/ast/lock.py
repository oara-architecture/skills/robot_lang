
class Lock(object):
    __islocked = False

    def __setattr__(self, key, value):
        if self.__islocked and key != '_Lock__islocked':
            raise TypeError( "%r is locked" % self )
        object.__setattr__(self, key, value)

    def _lock(self):
        self.__islocked = True

    def _unlock(self):
        self.__islocked = False
