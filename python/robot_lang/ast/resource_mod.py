from typing import List

from .lock import Lock
from .block import Block
from .info import Info

class Arc(Lock):
    """A resource arc.
    
    :param info: parsing information.
    :param extern: extern flag.
    :param src: arc source state.
    :param dst: arc destination state.
    """

    def __init__(self, info: Info, extern: bool, src: str, dst: str):
        self.__info = info
        self.__extern = extern
        self.__src = src
        self.__dst = dst
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def src(self) -> str:
        """Get arc source state."""
        return self.__src

    @property
    def dst(self) -> str:
        """Get arc destination state."""
        return self.__dst

    @property
    def extern(self) -> bool:
        """Get arc extern flag."""
        return self.__extern

    def __str__(self):
        return 'Arc'

    def __repr__(self):
        return 'Arc(' + str(self.info) + ')'

    def pretty(self):
        s =  '      '
        if self.extern:
            s += 'extern '
        s += str(self.src) + ' -> ' + str(self.dst)
        return s

    def json(self):
        s  = "\t\t\t\t\t\t\t{\n"
        if self.extern:
            s += "\t\t\t\t\t\t\t\t\"extern\": true,\n"
        else:
            s += "\t\t\t\t\t\t\t\t\"extern\": false,\n"
        s += "\t\t\t\t\t\t\t\t\"src\": \"" + str(self.src) + "\",\n"
        s += "\t\t\t\t\t\t\t\t\"dst\": \"" + str(self.dst) + "\"\n"
        s += "\t\t\t\t\t\t\t}\n"
        return s


class StateMachine(Lock):
    """A resource state machine.
    
    :param info: parsing information.
    :param initial: initial state.
    :param arcs: state-machine transitions.
    """

    def __init__(self, info: Info, initial: str, arcs: List[Arc]):
        self.__info = info
        self.__initial = initial
        self.__arcs = arcs
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def initial(self) -> str:
        """Get initial state."""
        return self.__initial

    @property
    def arcs(self) -> List[Arc]:
        """Get state-machine arcs."""
        return self.__arcs

    @property
    def states(self) -> List[str]:
        """Get state-machine states."""
        return list( {self.initial} | set(a.src for a in self.arcs) | set(a.dst for a in self.arcs) )

    def __str__(self):
        return 'StateMachine'

    def __repr__(self):
        return 'StateMachine(' + repr(self.initial) + ', ' + repr(self.arcs) + ', ' + str(self.info) + ')'

    def pretty(self):
        s = '      initial ' + str(self.initial) + '\n'
        for a in self.arcs:
            s += a.pretty() + '\n'
        return s

    def json(self):
        s =  "\t\t\t\t\t\"statemachine\": {\n"
        s += "\t\t\t\t\t\t\"initial\": \"" + str(self.initial) + "\",\n"
        if self.arcs != []:
            s += "\t\t\t\t\t\t\"arcs\": [\n"
            s += self.arcs[0].json()
            for a in self.arcs[1:]:
                s += "\t\t\t\t\t\t\t,\n"
                s += a.json()
            s += "\t\t\t\t\t\t]\n"
        else:
            s += "\t\t\t\t\t\t\"arcs\": []\n"
        s += "\t\t\t\t\t}\n"
        return s


class Resource(Lock):
    """A resource.
    
    :param info: parsing information.
    :param name: resource name.
    :param statemachine: resource state-machine model.
    """

    def __init__(self, info: Info, name: str, statemachine: StateMachine):
        self.__info = info
        self.__name = name
        self.__statemachine = statemachine
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get resource name."""
        return self.__name

    @property
    def statemachine(self) -> StateMachine:
        """Get resource state-machine."""
        return self.__statemachine

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Resource(' + repr(self.name) + ', ' + str(self.info) + ')'

    def __eq__(self, other):
        return str(self) == str(other)

    def __ge__(self, other):
        return str(self) >= str(other)

    def __gt__(self, other):
        return str(self) > str(other)

    def __le__(self, other):
        return str(self) <= str(other)

    def __lt__(self, other):
        return str(self) < str(other)

    def pretty(self):
        s = '    ' + str(self.name) + ' {\n'
        s += self.statemachine.pretty()
        s += '    }\n'
        return s

    def json(self):
        s  = "\t\t\t\t{\n"
        s += "\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        s += self.statemachine.json()
        s += "\t\t\t\t}\n"
        return s


class ResourceBlock(Block):
    """A block of resources."""

    def __str__(self):
        return 'ResourceBlock'

    def pretty(self):
        s = '  resource {\n'
        for r in self.list:
            s += r.pretty()
        s += '  }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\"resources\": [\n"
            s += self.list[0].json()
            for d in self.list[1:]:
                s += "\t\t\t\t,\n"
                s += d.json()
            s += "\t\t\t],\n"
        else:
            s += "\t\t\t\"resources\": [],\n"
        return s
