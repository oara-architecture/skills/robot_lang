from .lock import Lock
from .block import Block
from .info import Info

class Data(Lock):
    """A data.
    
    :param info: parsing information.
    :param name: data name.
    :param data_type: data type name.
    """

    def __init__(self, info: Info, name: str, data_type: str):
        self.__info = info
        self.__name = name
        self.__type = data_type
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing info."""
        return self.__info

    @property
    def name(self) -> str:
        """Get data name."""
        return self.__name

    @property
    def type(self) -> str:
        """Get data type."""
        return self.__type

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Data(' + repr(self.name) + ', ' + repr(self.type) + ', ' + str(self.info) + ')'

    def pretty(self):
        return str(self.name) + ': ' + str(self.type)

    def json(self):
        s  = "\t\t\t\t{\n"
        s += "\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        s += "\t\t\t\t\t\"type\": \"" + str(self.type) + "\"\n"
        s += "\t\t\t\t}\n"
        return s


class DataBlock(Block):
    """A block of data."""

    def __str__(self):
        return 'DataBlock'

    def pretty(self):
        s = '  data {\n'
        for d in self.list:
            s += '    ' + d.pretty() + '\n'
        s += '  }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\"data\": [\n"
            s += self.list[0].json()
            for d in self.list[1:]:
                s += "\t\t\t\t,\n"
                s += d.json()
            s += "\t\t\t],\n"
        else:
            s += "\t\t\t\"data\": [],\n"
        return s
