from abc import ABC
from typing import Union, List

from .lock import Lock
from .info import Info


class Expression(Lock, ABC):
    pass


class BinComp(Expression):
    """Binary expression comparison.

    :param info: parsing information.
    :param op: comparison operator ('<', '>', '<=', '>=', '==', '!=').
    :param left: left expression of the comparison expresion.
    :param right: right expression of the comparison expresion.
    """
    def __init__(self, info: Info, op: str, left: Expression, right: Expression):
        self.info = info
        self.op = op
        self.left = left
        self.right = right
        self._lock()

    def __str__(self):
        return '(' + str(self.left) + ' ' + str(self.op) + ' ' + str(self.right) + ')'

    def __repr__(self):
        return 'BinComp(' + repr(self.op) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \""
        if self.op == ">" : s += "GT"
        if self.op == ">=": s += "GE"
        if self.op == "<" : s += "LT"
        if self.op == "<=": s += "LE"
        if self.op == "==": s += "EQ"
        if self.op == "!=": s += "NE"
        s += "\", "
        s += "\"left\": " + self.left.json() + ", "
        s += "\"right\": " + self.right.json()
        s += " }"
        return s

class UnaryBool(Expression):
    """Unary boolean expression.

    :param info: parsing information.
    :param op: expression operator ('!').
    :param expr: expression of the boolean expresion.
    """
    def __init__(self, info: Info, op: str, expr: Expression):
        self.info = info
        self.op = op
        self.expr = expr
        self._lock()

    def __str__(self):
        return '(' + str(self.op) + ' ' + str(self.expr) + ')'

    def __repr__(self):
        return 'UnaryBool(' + repr(self.op) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \""
        s += "!"
        s += "\", "
        s += "\"expr\": " + self.expr.json()
        s += " }"
        return s

class BinBool(Expression):
    """Binary boolean expression.

    :param info: parsing information.
    :param op: expression operator ('&&', '||', '=>').
    :param left: left expression of the boolean expresion.
    :param right: right expression of the boolean expresion.
    """
    def __init__(self, info: Info, op: str, left: Expression, right: Expression):
        self.info = info
        self.op = op
        self.left = left
        self.right = right
        self._lock()

    def __str__(self):
        return '(' + str(self.left) + ' ' + str(self.op) + ' ' + str(self.right) + ')'

    def __repr__(self):
        return 'BinBool(' + repr(self.op) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \""
        if self.op == "&&": s += "And"
        if self.op == "||": s += "Or"
        if self.op == "=>": s += "Implies"
        s += "\", "
        s += "\"left\": " + self.left.json() + ", "
        s += "\"right\": " + self.right.json()
        s += " }"
        return s

class BoolValue(Expression):
    """ Boolean value expression.

    :param info: parsing information.
    :param value: boolean value (true of false).
    """
    def __init__(self, info: Info, value: bool):
        self.info = info
        self.value = value
        self._lock()

    def __str__(self):
        if self.value:
            return 'true'
        else:
            return 'false'

    def __repr__(self):
        return 'BoolValue(' + repr(self.value) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \"BoolValue\", "
        s += "\"value\": "
        if self.value:
            s += "true"
        else:
            s += "false"
        s += " }"
        return s


class Number(Expression):
    """ Number expression.

    :param info: parsing information.
    :param value: number value.
    """
    def __init__(self, info: Info, value: Union[int, float]):
        self.info = info
        self.value = value
        self._lock()

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return 'Number(' + repr(self.value) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \"Number\", "
        s += "\"value\": "
        s += str(self.value)
        s += " }"
        return s


class FunCall(Expression):
    """ Function call expression.

    :param info: parsing information.
    :param function: function name.
    :param parameters: list of function parameter expressions.
    """
    def __init__(self, info: Info, function: str, parameters: List[Expression]):
        self.info = info
        self.function = function
        self.parameters = parameters
        self._lock()

    def __str__(self):
        s = str(self.function) + '('
        if self.parameters != []:
            head, *tail = self.parameters
            s += str(head)
            for p in tail:
                s += ', ' + str(p)
        s += ')'
        return s

    def __repr__(self):
        return 'FunCall(' + repr(self.function) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \"FunCall\", "
        s += "\"function\": \"" + str(self.function) + "\", "
        s += "\"parameters\": ["
        if self.parameters != []:
            s += self.parameters[0].json()
            for p in s.parameters[1:]:
                s += ", " + p.json()
        s += "]"
        s += " }"
        return s


class Infinite(Expression):
    """ Infinite value expression.

    :param info: parsing information.
    """
    def __init__(self, info: Info):
        self.info = info
        self._lock()

    def __str__(self):
        return 'inf'

    def __repr__(self):
        return 'Infinite(' + str(self.info) + ')'

    def json(self):
        return "{ \"type\": \"Infinite\" }"


class Identifier(Expression):
    """ Identifier expression.

    :param info: parsing information.
    :param name: identifier expression value.
    """
    def __init__(self, info: Info, name: str):
        self.info = info
        self.name = name
        self._lock()

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Identifier(' + repr(self.name) + ', ' + str(self.info) + ')'

    def json(self):
        s  = "{ \"type\": \"Identifier\", "
        s += "\"name\": \"" + str(self.name) + "\""
        s += " }"
        return s

