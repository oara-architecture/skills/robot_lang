from typing import Any, Union, List

from .lock import Lock
from .block import Block
from .info import Info
from .effect import Effect
from .resource_expression import ResourceExpression
from .expression import Expression


class Result(Lock):
    """A terminal result of the skill.

    :param info: parsing information.
    :param name: result name.
    :param expression: logical expression checked when reaching this result.
    :param duration: expression specifying the duration to reach this result.
    :param post: resource expression checked when reaching this result.
    :param effects: name of the effects to apply when reaching this result.
    """

    def __init__(self, info: Info, name: str,
                 expression: Expression,
                 duration: Expression,
                 post: ResourceExpression,
                 effects: List[str]):
        self.__info = info
        self.__name = name
        self.__expression = expression
        self.__duration = duration
        self.__post = post
        self.__effects = effects
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get result name."""
        return self.__name

    @property
    def expression(self) -> Any:
        """Get result logic postcondition ."""
        return self.__expression

    @property
    def post(self) -> ResourceExpression:
        """Get result resource postcondition."""
        return self.__post

    @property
    def effects(self) -> List[str]:
        """Get result effects."""
        return self.__effects

    @property
    def duration(self) -> Any:
        """Get result duration."""
        return self.__duration

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Result(' + repr(self.name) + ', ' + str(self.info) + ')'

    def pretty(self):
        s = '        ' + str(self.name)
        s += ' {\n'
        if self.expression:
            s += '          ' + str(self.expression) + '\n'
        if self.duration:
            s += '          duration ' + str(self.duration) + '\n'
        if self.post:
            s += '          post     ' + str(self.post) + '\n'
        if self.effects:
            s += '          apply   '
            for e in self.effects:
                s += str(e) + ' '
            s += '\n'
        s += '        }\n'
        return s

    def json(self):
        s = "\t\t\t\t\t\t{\n"
        s += "\t\t\t\t\t\t\t\"name\": \"" + str(self.name) + "\""
        if self.expression or self.duration or self.effects:
            s += ","
        s += "\n"
        if self.expression:
            s += "\t\t\t\t\t\t\t\"expression\": " + self.expression.json()
            if self.duration or self.post or self.effects:
                s += ",\n"
            else:
                s += "\n"
        if self.duration:
            s += "\t\t\t\t\t\t\t\"duration\": " + self.duration.json()
            if self.post or self.effects:
                s += ",\n"
            else:
                s += "\n"
        if self.effects:
            s += "\t\t\t\t\t\t\t\"effect\": " + str(self.effects) + "\n"
        s += "\t\t\t\t\t\t}\n"
        return s


class ResultBlock(Block):
    """A block of results."""

    def __str__(self):
        return 'ResultBlock'

    def pretty(self):
        if self.list == []:
            return ''
        s = '      result {\n'
        for m in self.list:
            s += m.pretty()
        s += '      }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\t\t\"results\": [\n"
            s += self.list[0].json()
            for m in self.list[1:]:
                s += "\t\t\t\t\t\t,\n"
                s += m.json()
            s += "\t\t\t\t\t]\n"
        else:
            s += "\t\t\t\t\t\"results\": []\n"
        return s
