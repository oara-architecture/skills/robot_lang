from abc import ABC, abstractmethod
from typing import Optional, List, TypeVar
T = TypeVar('T')

from .lock import Lock

class Block(Lock, ABC):
    """A block of model elements.
    
    A block is iterable and has a length.
    """

    def __init__(self, elements:Optional[List[T]] = None):
        self.list = [] if elements is None else elements
        for e in self.list:
            try:
                setattr(self, e.name, e)
            except AttributeError:
                pass
        self._lock()

    def __iter__(self):
        return iter(self.list)

    def __len__(self):
        return len(self.list)

    def __getitem__(self, key: str) -> T:
        return getattr(self, key, None)

    @abstractmethod
    def __str__(self):
        return 'Block'

    def __repr__(self):
        return f"{str(self)}({repr(self.list)})"

    @abstractmethod
    def pretty(self):
        return repr(self)
