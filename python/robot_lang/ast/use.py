from .lock import Lock
from .block import Block

class Use(Lock):

    def __init__(self, info, resource, pre, start, inv):
        self.info = info
        self.resource = resource
        self.pre = pre
        self.start = start
        self.inv = inv
        self._lock()

    def __str__(self):
        return 'Use'

    def __repr__(self):
        return 'Use(' + repr(self.resource) + ', ' + str(self.info) + ')'

    def pretty(self):
        s = '        ' + str(self.resource)
        if self.pre:
            s += ' pre = ' + str(self.pre)
        if self.start:
            s += ' start = ' + str(self.start)
        if self.inv:
            s += ' inv = ' + str(self.inv)
        s += '\n'
        return s

class UseBlock(Block):

    def __str__(self):
        return 'UseBlock'

    def pretty(self):
        if self.list == []:
            return ''
        s = '      use {\n'
        for u in self.list:
            s += u.pretty()
        s += '      }\n'
        return s
