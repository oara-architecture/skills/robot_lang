from .lock import Lock


class NotUseExp(Lock):

    def __init__(self, info, name):
        self.info = info
        self.name = name
        self._lock()

    def __str__(self):
        return '(not ' + str(self.name) + ')'

    def __repr__(self):
        return 'NotUseExp(' + repr(self.name) + ', ' + str(self.info) + ')'

class DisjUseExp(Lock):

    def __init__(self, info, names):
        self.info = info
        self.names = names
        self._lock()

    def __str__(self):
        s = '(' + str(self.names[0])
        for n in self.names[1:]:
            s += ' or ' + str(n)
        s += ')'
        return s

    def __repr__(self):
        return 'DisjUseExp(' + repr(self.names) + ', ' + str(self.info) + ')'

class IdentUseExp(Lock):

    def __init__(self, info, name):
        self.info = info
        self.name = name
        self._lock()

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'IdentUseExp(' + repr(self.name) + ', ' + str(self.info) + ')'
