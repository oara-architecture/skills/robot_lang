from typing import List
from .lock import Lock
from .block import Block
from .info import Info

class ResourceEffect(Lock):
    """An effect on a resource.
    
    :param info: parsing information.
    :param resource: resource name.
    :param state: state to change the resource to.
    """

    def __init__(self, info: Info, resource: str, state: str):
        self.__info = info
        self.__resource = resource
        self.__state = state
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def resource(self) -> str:
        """Get resource."""
        return self.__resource

    @property
    def state(self) -> str:
        """Get effect state."""
        return self.__state

    def __str__(self):
        return str(self.resource) + ' -> ' + str(self.state)

    def __repr__(self):
        return 'ResourceEffect(' + repr(self.resource) + ', ' + repr(self.state) + ', ' + str(self.info) + ')'

    def pretty(self):
        return str(self)

    def json(self):
        s  = "\t\t\t\t\t\t\t\t{\n"
        s += "\t\t\t\t\t\t\t\t\t\"resource\": \"" + str(self.resource) + "\",\n"
        s += "\t\t\t\t\t\t\t\t\t\"state\": \"" + str(self.state) + "\"\n"
        s += "\t\t\t\t\t\t\t\t}\n"
        return s
       


class Effect(Lock):
    """An effect.
    
    An effect is defined by a set of ResourceEffects.
    
    :param info: parsing information.
    :param name: effect name.
    :param effects: list of resource effects.
    """

    def __init__(self, info: Info, name: str, effects: List[ResourceEffect]):
        self.__info = info
        self.__name = name
        self.__effects = effects
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get effect label."""
        return self.__name

    @property
    def effects(self) -> List[ResourceEffect]:
        """Get resource effects."""
        return self.__effects

    def __iter__(self):
        return iter(self.effects)

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Effect(' + repr(self.name) + ', ' + repr(self.effects) + ', ' + str(self.info) + ')'
    
    def pretty(self):
        s = '        ' + str(self.name)
        if self.effects == []:
            return s + ' {}\n'
        s += ' {\n'
        for e in self.effects:
            s += '          ' + e.pretty() + '\n'
        s += '        }\n'
        return s

    def json(self):
        s  = "\t\t\t\t\t\t{\n"
        s += "\t\t\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        if self.effects != []:
            s += "\t\t\t\t\t\t\t\"transitions\": [\n"
            s += self.effects[0].json()
            for e in self.effects[1:]:
                s += "\t\t\t\t\t\t\t\t,\n"
                s += e.json()
            s += "\t\t\t\t\t\t\t]\n"
        else:
            s += "\t\t\t\t\t\t\t\"transitions\": []\n"
        s += "\t\t\t\t\t\t}\n"
        return s


class EffectBlock(Block):
    """A block of effects."""

    def __str__(self):
        return 'EffectBlock'

    def pretty(self):
        if self.list == []:
            return ''
        s = '      effect {\n'
        for i in self.list:
            s += i.pretty()
        s += '      }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\t\t\"effects\": [\n"
            s += self.list[0].json()
            for e in self.list[1:]:
                s += "\t\t\t\t\t\t,\n"
                s += e.json()
            s += "\t\t\t\t\t],\n"
        else:
            s += "\t\t\t\t\t\"effects\": [],\n"
        return s
