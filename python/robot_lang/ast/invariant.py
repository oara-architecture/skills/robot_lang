from typing import Any, Union

from .lock import Lock
from .block import Block
from .info import Info
from .resource_expression import ResourceExpression
from .expression import Expression


class Invariant(Lock):
    """An invariant.
    
    :param info: parsing information.
    :param name: invariant name.
    :param expression: logical expression attached to this invariant.
    :param resource: resource expression attached to this invariant.
    :param effect: name of the effect to apply in case of invariant violation.
    """
    def __init__(self, info: Info, name: str, 
                 expression: Expression, resource: ResourceExpression, 
                 effect: str):
        self.__info = info
        self.__name = name
        self.__expression = expression
        self.__resource = resource
        self.__effect = effect
        self._lock()

    @property
    def info(self) -> Info:
        """Get parsing information."""
        return self.__info

    @property
    def name(self) -> str:
        """Get invariant name."""
        return self.__name

    @property
    def expression(self) -> Expression:
        """Get invariant logic expression."""
        return self.__expression

    @property
    def resource(self) -> ResourceExpression:
        """Get invariant resource expression."""
        return self.__resource

    @property
    def effect(self) -> str:
        """Get invariant violation effect."""
        return self.__effect

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return 'Invariant(' + str(self.info) + ')'

    def pretty(self):
        s = '        ' + str(self.name) + '{\n'
        if self.expression != None:           
            s += '          ' + str(self.expression) + '\n'
        if self.resource != None:
            s += '          resource  ' + str(self.resource) + '\n'
        if self.effect != None:
            s += '          violation ' + str(self.effect) + '\n'
        s += '        }\n'
        return s

    def json(self):
        s  = "\t\t\t\t\t\t{\n"
        s += "\t\t\t\t\t\t\t\"name\": \"" + str(self.name) + "\",\n"
        if self.expression:
            s += "\t\t\t\t\t\t\t\"expression\": " + self.expression.json()
            if self.resource or self.effect:
                s += ",\n"
            else:
                s += "\n"
        if self.resource:
            s += "\t\t\t\t\t\t\t\"resource\": " + self.resource.json()
            if self.effect:
                s += ",\n"
            else:
                s += "\n"
        if self.effect:
            s += "\t\t\t\t\t\t\t\"violation\": \"" + str(self.effect) + "\"\n"
        s += "\t\t\t\t\t\t}\n"
        return s


class InvariantBlock(Block):
    """A block of invariants."""

    def __str__(self):
        return 'InvariantBlock'

    def pretty(self):
        if self.list == []:
            return ''
        s = '      invariant {\n'
        for p in self.list:
            s += p.pretty()
        s += '      }\n'
        return s

    def json(self):
        s = ""
        if self.list != []:
            s += "\t\t\t\t\t\"invariants\": [\n"
            s += self.list[0].json()
            for p in self.list[1:]:
                s += "\t\t\t\t\t\t,\n"
                s += p.json()
            s += "\t\t\t\t\t],\n"
        else:
            s += "\t\t\t\t\t\"invariants\": [],\n"
        return s
