import argparse
import logging
import sys
import traceback

from .utils.parse_functions import parse_file
from .utils.logger import setup_logging
from .utils.exceptions import RobotLangException
from .ast.model import Model
from .utils.dot_generator import ResourceDot, SkillDot

def main():
    parser = argparse.ArgumentParser(prog='python -m robot_lang',
        description='Robot skills language linter')
    parser.add_argument('file', nargs='+', type=str, help='model file')
    parser.add_argument("-d", "--debug", help="Activate debug logs",
                        action='store_const', dest="loglevel",
                        const=logging.DEBUG, default=logging.WARNING)
    parser.add_argument("-v", "--verbose", help="Activate verbose logs",
                        action='store_const', dest="loglevel",
                        const=logging.INFO, default=logging.WARNING)
    parser.add_argument('--dot', action='store_true', help='generate graphviz views of the models')
    args = parser.parse_args()
    setup_logging(level=args.loglevel)

    logging.getLogger('robot_lang').info(f"Parsing files {', '.join(args.file)}")

    global_model = Model()
    for model_file in args.file:
        logging.getLogger('robot_lang').debug(f"Parsing file {model_file}")
        try:
            tree, model = parse_file(model_file)
        except RobotLangException:
            sys.exit(1)
        except Exception as e:
            logging.getLogger('robot_lang').error(e)
            traceback.print_exc()
            sys.exit(1)
        logging.getLogger('robot_lang').debug(f" - tree:\n{tree.toStringTree()}")
        if len(args.file) > 1:
            logging.getLogger('robot_lang').debug(f" - model:\n{model.pretty()}")
        global_model.extend(model)
    
    logging.getLogger('robot_lang').info(f"Parsed model:\n{global_model.pretty()}")

    if args.dot:
        for skillset in global_model.skillsets:
            for resource in skillset.resources:
                logging.getLogger('robot_lang').info(f"generating resource dot file {skillset}_{resource}.dot")
                ResourceDot(resource).write(f"{skillset}_{resource}.dot")
            for skill in skillset.skills:
                logging.getLogger('robot_lang').info(f"generating skill dot file {skillset}_{skill}.dot")
                SkillDot(skill).write(f"{skillset}_{skill}.dot")


if __name__ == '__main__':
    main()
