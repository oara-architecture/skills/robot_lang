from ..parser.SkillsLexer import SkillsLexer
from ..parser.SkillsParser import SkillsParser
from ..parser.SkillsVisitor import SkillsVisitor

from ..ast.info import Info
from ..ast.model import Model
from ..ast.type import *
from ..ast.skillset import *
from ..ast.data import *
from ..ast.function import *
from ..ast.resource_mod import *
from ..ast.skill import *
from ..ast.input import *
from ..ast.precondition import *
from ..ast.result import *
from ..ast.postcondition import *
from ..ast.use import *
from ..ast.use_exp import *
from ..ast.expression import *
from ..ast.effect import *
from ..ast.invariant import *
from ..ast.resource_expression import *


class Visitor(SkillsVisitor):

    def __init__(self, file):
        self.file = file

    def visitModel(self, ctx: SkillsParser.ModelContext):
        types = list(map(lambda x: self.visit(x), ctx.types))
        types_flat = [item for sublist in types for item in sublist]
        skillsets = list(map(lambda x: self.visit(x), ctx.skillsets))
        return Model(TypeBlock(types_flat), SkillSetBlock(skillsets))

    # ------------------------- Type -------------------------

    def visitTypeBlock(self, ctx: SkillsParser.TypeBlockContext):
        _ = Info(self.file, ctx.start.line, ctx.start.column)
        return list(map(lambda x: self.visit(x), ctx.types))

    def visitTypeDef(self, ctx: SkillsParser.TypeDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        return Type(info, ctx.name.text)

    # ------------------------- SkillSet -------------------------

    def visitSkillset(self, ctx: SkillsParser.SkillsetContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        # Data
        data = list(map(lambda x: self.visit(x), ctx.data))
        # data = [self.visit(x) for x in ctx.data]
        data_flat = [item for sublist in data for item in sublist]
        data = DataBlock(data_flat)
        # Function
        functions = list(map(lambda x: self.visit(x), ctx.functions))
        functions_flat = [item for sublist in functions for item in sublist]
        functions = FunctionBlock(functions_flat)
        # Resource
        resources = list(map(lambda x: self.visit(x), ctx.resources))
        resources_flat = [item for sublist in resources for item in sublist]
        resources = ResourceBlock(resources_flat)
        # Skill
        skills = list(map(lambda x: self.visit(x), ctx.skills))
        skills_flat = [item for sublist in skills for item in sublist]
        skills = SkillBlock(skills_flat)
        #
        return SkillSet(info, ctx.name.text, data, functions, resources, skills)

    # ------------------------- Data -------------------------

    def visitDataBlock(self, ctx: SkillsParser.DataBlockContext):
        _ = Info(self.file, ctx.start.line, ctx.start.column)
        return list(map(lambda x: self.visit(x), ctx.data))

    def visitDataDef(self, ctx: SkillsParser.DataDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        return Data(info, ctx.name.text, ctx.dataType.text)

    # ------------------------- Function -------------------------

    def visitFunctionBlock(self, ctx: SkillsParser.FunctionBlockContext):
        _ = Info(self.file, ctx.start.line, ctx.start.column)
        return list(map(lambda x: self.visit(x), ctx.functions))

    def visitFunctionDef(self, ctx: SkillsParser.FunctionDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        parameters = list(map(lambda x: self.visit(x), ctx.params))
        return Function(info, ctx.name.text, parameters,
                        ctx.funType if ctx.funType is None else ctx.funType.text)

    def visitParameterDef(self, ctx: SkillsParser.ParameterDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        return Parameter(info, ctx.name.text, ctx.paramType.text)

    # ------------------------- Resource -------------------------

    def visitResourceBlock(self, ctx: SkillsParser.ResourceBlockContext):
        _ = Info(self.file, ctx.start.line, ctx.start.column)
        return list(map(lambda x: self.visit(x), ctx.resources))

    def visitResourceDef(self, ctx: SkillsParser.ResourceDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        initial = self.visit(ctx.initial)
        arcs = list(map(lambda x: self.visit(x), ctx.arcs))
        return Resource(info, ctx.name.text, StateMachine(info, initial, arcs))

    # ------------------------- StateMachine -------------------------

    def visitResourceInitial(self, ctx: SkillsParser.ResourceInitialContext):
        return ctx.name.text

    def visitResourceArc(self, ctx: SkillsParser.ResourceArcContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        extern = False
        if ctx.is_extern != None:
            extern = True
        return Arc(info, extern, ctx.src.text, ctx.dst.text)

    # ------------------------- Skill -------------------------

    def visitSkillBlock(self, ctx: SkillsParser.SkillBlockContext):
        _ = Info(self.file, ctx.start.line, ctx.start.column)
        return list(map(lambda x: self.visit(x), ctx.skills))

    def visitSkillDef(self, ctx: SkillsParser.SkillDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        # Progress
        progress = float(ctx.progress.text)
        # Input
        inputs = InputBlock([])
        if ctx.inputs != None:
            inputs = self.visit(ctx.inputs)
        # Effect
        effects = EffectBlock([])
        if ctx.effects != None:
            effects = self.visit(ctx.effects)
        # Precondition
        preconds = PreconditionBlock([], None)
        if ctx.preconditions != None:
            preconds = self.visit(ctx.preconditions)
        # Invariant
        invariants = InvariantBlock([])
        if ctx.invariants != None:
            invariants = self.visit(ctx.invariants)
        # Interruption Effect
        interrupt_effect = None
        if ctx.interrupt_effect != None:
            interrupt_effect = ctx.interrupt_effect.text
        # result
        results = ResultBlock([])
        if ctx.results != None:
            results = self.visit(ctx.results)
        return Skill(info, ctx.name.text, progress, inputs, effects, preconds, invariants, results)

    # ------------------------- Input -------------------------

    def visitInputBlock(self, ctx: SkillsParser.InputBlockContext):
        # info = Info(self.file, ctx.start.line, ctx.start.column)
        inputs = list(map(lambda x: self.visit(x), ctx.inputs))
        return InputBlock(inputs)

    def visitInputDef(self, ctx: SkillsParser.InputDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        return Input(info, ctx.name.text, ctx.inputType.text)

    # ------------------------- Effect -------------------------

    def visitEffectBlock(self, ctx: SkillsParser.EffectBlockContext):
        # info = Info(self.file, ctx.start.line, ctx.start.column)
        effects = list(map(lambda x: self.visit(x), ctx.effects))
        return EffectBlock(effects)

    def visitEffectDef(self, ctx: SkillsParser.EffectDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        effects = list(map(lambda x: self.visit(x), ctx.effects))
        return Effect(info, ctx.name.text, effects)

    def visitResourceEffect(self, ctx: SkillsParser.ResourceEffectContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        return ResourceEffect(info, ctx.name.text, ctx.state.text)

    # ------------------------- Precondition -------------------------

    def visitPreconditionBlock(self, ctx: SkillsParser.PreconditionBlockContext):
        _ = Info(self.file, ctx.start.line, ctx.start.column)
        preconds = list(map(lambda x: self.visit(x), ctx.preconds))
        success = None
        if ctx.success != None:
            success = ctx.success.text
        return PreconditionBlock(preconds, success)

    def visitPreconditionDef(self, ctx: SkillsParser.PreconditionDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        name = ctx.name.text
        expr = None
        if ctx.expr != None:
            expr = self.visit(ctx.expr)
        resource = None
        if ctx.res_expr != None:
            resource = self.visit(ctx.res_expr)
        failure = None
        if ctx.failure != None:
            failure = ctx.failure.text
        return Precondition(info, name, expr, resource, failure)

    # ------------------------- Invariant -------------------------

    def visitInvariantBlock(self, ctx: SkillsParser.InvariantBlockContext):
        _ = Info(self.file, ctx.start.line, ctx.start.column)
        invariants = list(map(lambda x: self.visit(x), ctx.invariants))
        return InvariantBlock(invariants)

    def visitInvariantDef(self, ctx: SkillsParser.InvariantDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        name = ctx.name.text
        expr = None
        if ctx.expr != None:
            expr = self.visit(ctx.expr)
        resource = None
        if ctx.res_expr != None:
            resource = self.visit(ctx.res_expr)
        effect = None
        if ctx.effect != None:
            effect = ctx.effect.text
        return Invariant(info, name, expr, resource, effect)

    # ------------------------- Result -------------------------

    def visitResultBlock(self, ctx: SkillsParser.ResultBlockContext):
        # info = Info(self.file, ctx.start.line, ctx.start.column)
        results = list(map(lambda x: self.visit(x), ctx.results))
        return ResultBlock(results)

    def visitResultDef(self, ctx: SkillsParser.ResultDefContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        # Expression
        expression = None
        if ctx.expr != None:
            expression = self.visit(ctx.expr)
        # Duration
        duration = None
        if ctx.duration != None:
            duration = self.visit(ctx.duration)
        # Post
        post = None
        if ctx.post != None:
            post = self.visit(ctx.post)
        # Effect
        effects = None
        if ctx.effects != None:
            effects = [x.text for x in ctx.effects]
        #
        return Result(info, ctx.name.text, expression, duration, post, effects)

    # def visitResultDuration(self, ctx:SkillsParser.ResultDurationContext):
    #     return self.visit(ctx.expr)

    # def visitResultResource(self, ctx:SkillsParser.ResultResourceContext):
    #     info = Info(self.file, ctx.start.line, ctx.start.column)
    #     post = None
    #     if ctx.post != None:
    #         post = self.visit(ctx.post)
    #     effect = None
    #     if ctx.effect != None:
    #         effect = ctx.effect.text
    #     return ResultResource(info, ctx.name.text, post, effect)

    # ------------------------- Postcondition -------------------------

    # def visitPostconditionBlock(self, ctx:SkillsParser.PostconditionBlockContext):
    #     return list(map(lambda x: self.visit(x), ctx.postconds))

    # def visitPostconditionDef(self, ctx:SkillsParser.PostconditionDefContext):
    #     info = Info(self.file, ctx.start.line, ctx.start.column)
    #     expr = self.visit(ctx.expr)
    #     return Postcondition(info, expr)

    # ------------------------- Use -------------------------

    # def visitUseBlock(self, ctx:SkillsParser.UseBlockContext):
    #     return list(map(lambda x: self.visit(x), ctx.uses))

    # def visitUseDef(self, ctx:SkillsParser.UseDefContext):
    #     info = Info(self.file, ctx.start.line, ctx.start.column)
    #     resource = ctx.resource.text
    #     # pre
    #     pre = None
    #     if ctx.pre != None:
    #         pre = self.visit(ctx.pre)
    #     # start
    #     start = None
    #     if ctx.sta != None:
    #         start = ctx.sta.text
    #     # end
    #     inv = None
    #     if ctx.inv != None:
    #         inv = self.visit(ctx.inv)
    #     #
    #     return Use(info, resource, pre, start, inv)

    # ------------------------- UseExp -------------------------

    # def visitNotUseExp(self, ctx:SkillsParser.NotUseExpContext):
    #     info = Info(self.file, ctx.start.line, ctx.start.column)
    #     name = ctx.name.text
    #     return NotUseExp(info, name)

    # def visitDisjUseExp(self, ctx:SkillsParser.DisjUseExpContext):
    #     info = Info(self.file, ctx.start.line, ctx.start.column)
    #     names = list(map(lambda x: x.text, ctx.names))
    #     return DisjUseExp(info, names)

    # def visitIdentUseExp(self, ctx:SkillsParser.IdentUseExpContext):
    #     info = Info(self.file, ctx.start.line, ctx.start.column)
    #     name = ctx.name.text
    #     return IdentUseExp(info, name)

    # ------------------------- Expression -------------------------

    def visitBinCompExp(self, ctx: SkillsParser.BinCompExpContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        left = self.visit(ctx.left)
        right = self.visit(ctx.right)
        op = ctx.op.text
        return BinComp(info, op, left, right)

    def visitNotExp(self, ctx: SkillsParser.NotExpContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        expr = self.visit(ctx.expr)
        op = ctx.op.text
        return UnaryBool(info, op, expr)

    def visitAndExp(self, ctx: SkillsParser.AndExpContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        left = self.visit(ctx.left)
        right = self.visit(ctx.right)
        op = ctx.op.text
        return BinBool(info, op, left, right)

    def visitOrExp(self, ctx: SkillsParser.OrExpContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        left = self.visit(ctx.left)
        right = self.visit(ctx.right)
        op = ctx.op.text
        return BinBool(info, op, left, right)

    def visitImpliesExp(self, ctx: SkillsParser.ImpliesExpContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        left = self.visit(ctx.left)
        right = self.visit(ctx.right)
        op = ctx.op.text
        return BinBool(info, op, left, right)

    def visitParenExp(self, ctx: SkillsParser.ParenExpContext):
        return self.visit(ctx.expr)

    def visitFunCall(self, ctx: SkillsParser.FunCallContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        function = ctx.function.text
        parameters = list(map(lambda x: self.visit(x), ctx.params))
        return FunCall(info, function, parameters)

    def visitBoolValue(self, ctx: SkillsParser.BoolValueContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        if ctx.value.text == 'true':
            return BoolValue(info, True)
        else:
            return BoolValue(info, False)

    def visitNumber(self, ctx: SkillsParser.NumberContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        value = float(ctx.value.text)
        return Number(info, value)

    def visitInfinite(self, ctx: SkillsParser.InfiniteContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        return Infinite(info)

    def visitIdentifier(self, ctx: SkillsParser.IdentifierContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        name = ctx.name.text
        return Identifier(info, name)

    # ------------------------- Resource Expression -------------------------

    # def visitResEq(self, ctx:SkillsParser.ResEqContext):
    #     info = Info(self.file, ctx.start.line, ctx.start.column)
    #     name = ctx.name.text
    #     state = ctx.state.text
    #     return ResEq(info, name, state)

    def visitBinCompResExp(self, ctx: SkillsParser.BinCompResExpContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        resource = ctx.name.text
        state = ctx.state.text
        op = ctx.op.text
        return BinCompRes(info, op, resource, state)

    def visitBinBoolResExp(self, ctx: SkillsParser.BinBoolResExpContext):
        info = Info(self.file, ctx.start.line, ctx.start.column)
        left = self.visit(ctx.left)
        right = self.visit(ctx.right)
        op = ctx.op.text
        return BinBoolRes(info, op, left, right)

    def visitParenResExp(self, ctx: SkillsParser.ParenResExpContext):
        return self.visit(ctx.expr)
