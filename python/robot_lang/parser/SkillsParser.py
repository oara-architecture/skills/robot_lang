# Generated from Skills.g4 by ANTLR 4.13.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,54,498,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,
        2,14,7,14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,19,2,20,
        7,20,2,21,7,21,2,22,7,22,2,23,7,23,2,24,7,24,2,25,7,25,2,26,7,26,
        2,27,7,27,2,28,7,28,1,0,1,0,5,0,61,8,0,10,0,12,0,64,9,0,1,1,1,1,
        1,1,5,1,69,8,1,10,1,12,1,72,9,1,1,1,1,1,1,1,3,1,77,8,1,1,2,1,2,1,
        3,1,3,1,3,1,3,1,3,1,3,1,3,5,3,88,8,3,10,3,12,3,91,9,3,1,3,1,3,1,
        4,1,4,1,4,5,4,98,8,4,10,4,12,4,101,9,4,1,4,1,4,1,4,3,4,106,8,4,1,
        5,1,5,1,5,1,5,1,6,1,6,1,6,5,6,115,8,6,10,6,12,6,118,9,6,1,6,1,6,
        1,6,3,6,123,8,6,1,7,1,7,1,7,1,7,1,7,5,7,130,8,7,10,7,12,7,133,9,
        7,3,7,135,8,7,1,7,1,7,1,7,3,7,140,8,7,1,8,1,8,1,8,1,8,1,9,1,9,1,
        9,5,9,149,8,9,10,9,12,9,152,9,9,1,9,1,9,1,9,3,9,157,8,9,1,10,1,10,
        1,10,1,10,5,10,163,8,10,10,10,12,10,166,9,10,1,10,1,10,1,11,1,11,
        1,11,1,12,3,12,174,8,12,1,12,1,12,1,12,1,12,1,13,1,13,1,13,5,13,
        183,8,13,10,13,12,13,186,9,13,1,13,1,13,1,13,3,13,191,8,13,1,14,
        1,14,1,14,1,14,1,14,1,14,3,14,199,8,14,1,14,3,14,202,8,14,1,14,3,
        14,205,8,14,1,14,3,14,208,8,14,1,14,1,14,3,14,212,8,14,1,14,3,14,
        215,8,14,1,14,3,14,218,8,14,1,14,1,14,1,15,1,15,1,15,5,15,225,8,
        15,10,15,12,15,228,9,15,1,15,1,15,1,15,3,15,233,8,15,1,16,1,16,1,
        16,1,16,1,17,1,17,1,17,5,17,242,8,17,10,17,12,17,245,9,17,1,17,1,
        17,1,17,3,17,250,8,17,1,18,1,18,1,18,4,18,255,8,18,11,18,12,18,256,
        1,18,1,18,1,18,5,18,262,8,18,10,18,12,18,265,9,18,1,18,3,18,268,
        8,18,1,19,1,19,1,19,1,19,1,20,1,20,1,20,5,20,277,8,20,10,20,12,20,
        280,9,20,1,20,1,20,3,20,284,8,20,1,20,1,20,1,20,1,20,1,20,1,20,3,
        20,292,8,20,3,20,294,8,20,1,21,1,21,1,21,3,21,299,8,21,1,21,1,21,
        1,21,3,21,304,8,21,1,21,1,21,1,21,3,21,309,8,21,1,21,1,21,1,21,3,
        21,314,8,21,1,21,1,21,3,21,318,8,21,1,21,1,21,3,21,322,8,21,1,21,
        3,21,325,8,21,1,22,1,22,1,22,5,22,330,8,22,10,22,12,22,333,9,22,
        1,22,1,22,1,22,3,22,338,8,22,1,23,1,23,1,23,3,23,343,8,23,1,23,1,
        23,1,23,3,23,348,8,23,1,23,1,23,1,23,3,23,353,8,23,1,23,1,23,1,23,
        3,23,358,8,23,1,23,1,23,3,23,362,8,23,1,23,1,23,3,23,366,8,23,1,
        23,3,23,369,8,23,1,24,1,24,1,24,5,24,374,8,24,10,24,12,24,377,9,
        24,1,24,1,24,1,24,3,24,382,8,24,1,25,1,25,1,25,3,25,387,8,25,1,25,
        1,25,3,25,391,8,25,1,25,1,25,3,25,395,8,25,1,25,1,25,4,25,399,8,
        25,11,25,12,25,400,3,25,403,8,25,1,25,1,25,1,25,1,25,3,25,409,8,
        25,1,25,1,25,1,25,3,25,414,8,25,1,25,1,25,1,25,3,25,419,8,25,1,25,
        1,25,1,25,4,25,424,8,25,11,25,12,25,425,3,25,428,8,25,3,25,430,8,
        25,1,26,1,26,1,26,1,26,1,26,1,26,1,26,1,26,3,26,440,8,26,1,26,1,
        26,1,26,5,26,445,8,26,10,26,12,26,448,9,26,1,27,1,27,1,27,1,27,1,
        27,1,27,1,27,1,27,1,27,1,27,1,27,1,27,5,27,462,8,27,10,27,12,27,
        465,9,27,3,27,467,8,27,1,27,1,27,1,27,1,27,1,27,3,27,474,8,27,1,
        27,1,27,1,27,1,27,1,27,1,27,1,27,1,27,1,27,1,27,1,27,1,27,5,27,488,
        8,27,10,27,12,27,491,9,27,1,28,1,28,1,28,1,28,1,28,1,28,0,2,52,54,
        29,0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,
        44,46,48,50,52,54,56,0,5,1,0,49,50,1,0,40,41,1,0,43,45,1,0,24,25,
        1,0,36,41,550,0,62,1,0,0,0,2,76,1,0,0,0,4,78,1,0,0,0,6,80,1,0,0,
        0,8,105,1,0,0,0,10,107,1,0,0,0,12,122,1,0,0,0,14,124,1,0,0,0,16,
        141,1,0,0,0,18,156,1,0,0,0,20,158,1,0,0,0,22,169,1,0,0,0,24,173,
        1,0,0,0,26,190,1,0,0,0,28,192,1,0,0,0,30,232,1,0,0,0,32,234,1,0,
        0,0,34,249,1,0,0,0,36,267,1,0,0,0,38,269,1,0,0,0,40,293,1,0,0,0,
        42,324,1,0,0,0,44,337,1,0,0,0,46,368,1,0,0,0,48,381,1,0,0,0,50,429,
        1,0,0,0,52,439,1,0,0,0,54,473,1,0,0,0,56,492,1,0,0,0,58,61,3,2,1,
        0,59,61,3,6,3,0,60,58,1,0,0,0,60,59,1,0,0,0,61,64,1,0,0,0,62,60,
        1,0,0,0,62,63,1,0,0,0,63,1,1,0,0,0,64,62,1,0,0,0,65,66,5,1,0,0,66,
        70,5,28,0,0,67,69,3,4,2,0,68,67,1,0,0,0,69,72,1,0,0,0,70,68,1,0,
        0,0,70,71,1,0,0,0,71,73,1,0,0,0,72,70,1,0,0,0,73,77,5,29,0,0,74,
        75,5,1,0,0,75,77,3,4,2,0,76,65,1,0,0,0,76,74,1,0,0,0,77,3,1,0,0,
        0,78,79,5,51,0,0,79,5,1,0,0,0,80,81,5,2,0,0,81,82,5,51,0,0,82,89,
        5,28,0,0,83,88,3,8,4,0,84,88,3,12,6,0,85,88,3,18,9,0,86,88,3,26,
        13,0,87,83,1,0,0,0,87,84,1,0,0,0,87,85,1,0,0,0,87,86,1,0,0,0,88,
        91,1,0,0,0,89,87,1,0,0,0,89,90,1,0,0,0,90,92,1,0,0,0,91,89,1,0,0,
        0,92,93,5,29,0,0,93,7,1,0,0,0,94,95,5,3,0,0,95,99,5,28,0,0,96,98,
        3,10,5,0,97,96,1,0,0,0,98,101,1,0,0,0,99,97,1,0,0,0,99,100,1,0,0,
        0,100,102,1,0,0,0,101,99,1,0,0,0,102,106,5,29,0,0,103,104,5,3,0,
        0,104,106,3,10,5,0,105,94,1,0,0,0,105,103,1,0,0,0,106,9,1,0,0,0,
        107,108,5,51,0,0,108,109,5,35,0,0,109,110,5,51,0,0,110,11,1,0,0,
        0,111,112,5,4,0,0,112,116,5,28,0,0,113,115,3,14,7,0,114,113,1,0,
        0,0,115,118,1,0,0,0,116,114,1,0,0,0,116,117,1,0,0,0,117,119,1,0,
        0,0,118,116,1,0,0,0,119,123,5,29,0,0,120,121,5,4,0,0,121,123,3,14,
        7,0,122,111,1,0,0,0,122,120,1,0,0,0,123,13,1,0,0,0,124,125,5,51,
        0,0,125,134,5,26,0,0,126,131,3,16,8,0,127,128,5,33,0,0,128,130,3,
        16,8,0,129,127,1,0,0,0,130,133,1,0,0,0,131,129,1,0,0,0,131,132,1,
        0,0,0,132,135,1,0,0,0,133,131,1,0,0,0,134,126,1,0,0,0,134,135,1,
        0,0,0,135,136,1,0,0,0,136,139,5,27,0,0,137,138,5,35,0,0,138,140,
        5,51,0,0,139,137,1,0,0,0,139,140,1,0,0,0,140,15,1,0,0,0,141,142,
        5,51,0,0,142,143,5,35,0,0,143,144,5,51,0,0,144,17,1,0,0,0,145,146,
        5,7,0,0,146,150,5,28,0,0,147,149,3,20,10,0,148,147,1,0,0,0,149,152,
        1,0,0,0,150,148,1,0,0,0,150,151,1,0,0,0,151,153,1,0,0,0,152,150,
        1,0,0,0,153,157,5,29,0,0,154,155,5,7,0,0,155,157,3,20,10,0,156,145,
        1,0,0,0,156,154,1,0,0,0,157,19,1,0,0,0,158,159,5,51,0,0,159,160,
        5,28,0,0,160,164,3,22,11,0,161,163,3,24,12,0,162,161,1,0,0,0,163,
        166,1,0,0,0,164,162,1,0,0,0,164,165,1,0,0,0,165,167,1,0,0,0,166,
        164,1,0,0,0,167,168,5,29,0,0,168,21,1,0,0,0,169,170,5,5,0,0,170,
        171,5,51,0,0,171,23,1,0,0,0,172,174,5,8,0,0,173,172,1,0,0,0,173,
        174,1,0,0,0,174,175,1,0,0,0,175,176,5,51,0,0,176,177,5,6,0,0,177,
        178,5,51,0,0,178,25,1,0,0,0,179,180,5,9,0,0,180,184,5,28,0,0,181,
        183,3,28,14,0,182,181,1,0,0,0,183,186,1,0,0,0,184,182,1,0,0,0,184,
        185,1,0,0,0,185,187,1,0,0,0,186,184,1,0,0,0,187,191,5,29,0,0,188,
        189,5,9,0,0,189,191,3,28,14,0,190,179,1,0,0,0,190,188,1,0,0,0,191,
        27,1,0,0,0,192,193,5,51,0,0,193,194,5,28,0,0,194,195,5,22,0,0,195,
        196,5,46,0,0,196,198,7,0,0,0,197,199,3,30,15,0,198,197,1,0,0,0,198,
        199,1,0,0,0,199,201,1,0,0,0,200,202,3,34,17,0,201,200,1,0,0,0,201,
        202,1,0,0,0,202,204,1,0,0,0,203,205,3,40,20,0,204,203,1,0,0,0,204,
        205,1,0,0,0,205,207,1,0,0,0,206,208,3,44,22,0,207,206,1,0,0,0,207,
        208,1,0,0,0,208,214,1,0,0,0,209,211,5,20,0,0,210,212,5,15,0,0,211,
        210,1,0,0,0,211,212,1,0,0,0,212,213,1,0,0,0,213,215,5,51,0,0,214,
        209,1,0,0,0,214,215,1,0,0,0,215,217,1,0,0,0,216,218,3,48,24,0,217,
        216,1,0,0,0,217,218,1,0,0,0,218,219,1,0,0,0,219,220,5,29,0,0,220,
        29,1,0,0,0,221,222,5,10,0,0,222,226,5,28,0,0,223,225,3,32,16,0,224,
        223,1,0,0,0,225,228,1,0,0,0,226,224,1,0,0,0,226,227,1,0,0,0,227,
        229,1,0,0,0,228,226,1,0,0,0,229,233,5,29,0,0,230,231,5,10,0,0,231,
        233,3,32,16,0,232,221,1,0,0,0,232,230,1,0,0,0,233,31,1,0,0,0,234,
        235,5,51,0,0,235,236,5,35,0,0,236,237,5,51,0,0,237,33,1,0,0,0,238,
        239,5,15,0,0,239,243,5,28,0,0,240,242,3,36,18,0,241,240,1,0,0,0,
        242,245,1,0,0,0,243,241,1,0,0,0,243,244,1,0,0,0,244,246,1,0,0,0,
        245,243,1,0,0,0,246,250,5,29,0,0,247,248,5,15,0,0,248,250,3,36,18,
        0,249,238,1,0,0,0,249,247,1,0,0,0,250,35,1,0,0,0,251,252,5,51,0,
        0,252,254,5,35,0,0,253,255,3,38,19,0,254,253,1,0,0,0,255,256,1,0,
        0,0,256,254,1,0,0,0,256,257,1,0,0,0,257,268,1,0,0,0,258,259,5,51,
        0,0,259,263,5,28,0,0,260,262,3,38,19,0,261,260,1,0,0,0,262,265,1,
        0,0,0,263,261,1,0,0,0,263,264,1,0,0,0,264,266,1,0,0,0,265,263,1,
        0,0,0,266,268,5,29,0,0,267,251,1,0,0,0,267,258,1,0,0,0,268,37,1,
        0,0,0,269,270,5,51,0,0,270,271,5,6,0,0,271,272,5,51,0,0,272,39,1,
        0,0,0,273,274,5,11,0,0,274,278,5,28,0,0,275,277,3,42,21,0,276,275,
        1,0,0,0,277,280,1,0,0,0,278,276,1,0,0,0,278,279,1,0,0,0,279,283,
        1,0,0,0,280,278,1,0,0,0,281,282,5,16,0,0,282,284,5,51,0,0,283,281,
        1,0,0,0,283,284,1,0,0,0,284,285,1,0,0,0,285,294,5,29,0,0,286,287,
        5,11,0,0,287,291,3,42,21,0,288,289,5,16,0,0,289,290,5,46,0,0,290,
        292,5,51,0,0,291,288,1,0,0,0,291,292,1,0,0,0,292,294,1,0,0,0,293,
        273,1,0,0,0,293,286,1,0,0,0,294,41,1,0,0,0,295,296,5,51,0,0,296,
        298,5,35,0,0,297,299,3,54,27,0,298,297,1,0,0,0,298,299,1,0,0,0,299,
        303,1,0,0,0,300,301,5,7,0,0,301,302,5,46,0,0,302,304,3,52,26,0,303,
        300,1,0,0,0,303,304,1,0,0,0,304,308,1,0,0,0,305,306,5,17,0,0,306,
        307,5,46,0,0,307,309,5,51,0,0,308,305,1,0,0,0,308,309,1,0,0,0,309,
        325,1,0,0,0,310,311,5,51,0,0,311,313,5,28,0,0,312,314,3,54,27,0,
        313,312,1,0,0,0,313,314,1,0,0,0,314,317,1,0,0,0,315,316,5,7,0,0,
        316,318,3,52,26,0,317,315,1,0,0,0,317,318,1,0,0,0,318,321,1,0,0,
        0,319,320,5,17,0,0,320,322,5,51,0,0,321,319,1,0,0,0,321,322,1,0,
        0,0,322,323,1,0,0,0,323,325,5,29,0,0,324,295,1,0,0,0,324,310,1,0,
        0,0,325,43,1,0,0,0,326,327,5,18,0,0,327,331,5,28,0,0,328,330,3,46,
        23,0,329,328,1,0,0,0,330,333,1,0,0,0,331,329,1,0,0,0,331,332,1,0,
        0,0,332,334,1,0,0,0,333,331,1,0,0,0,334,338,5,29,0,0,335,336,5,18,
        0,0,336,338,3,46,23,0,337,326,1,0,0,0,337,335,1,0,0,0,338,45,1,0,
        0,0,339,340,5,51,0,0,340,342,5,35,0,0,341,343,3,54,27,0,342,341,
        1,0,0,0,342,343,1,0,0,0,343,347,1,0,0,0,344,345,5,7,0,0,345,346,
        5,46,0,0,346,348,3,52,26,0,347,344,1,0,0,0,347,348,1,0,0,0,348,352,
        1,0,0,0,349,350,5,19,0,0,350,351,5,46,0,0,351,353,5,51,0,0,352,349,
        1,0,0,0,352,353,1,0,0,0,353,369,1,0,0,0,354,355,5,51,0,0,355,357,
        5,28,0,0,356,358,3,54,27,0,357,356,1,0,0,0,357,358,1,0,0,0,358,361,
        1,0,0,0,359,360,5,7,0,0,360,362,3,52,26,0,361,359,1,0,0,0,361,362,
        1,0,0,0,362,365,1,0,0,0,363,364,5,19,0,0,364,366,5,51,0,0,365,363,
        1,0,0,0,365,366,1,0,0,0,366,367,1,0,0,0,367,369,5,29,0,0,368,339,
        1,0,0,0,368,354,1,0,0,0,369,47,1,0,0,0,370,371,5,12,0,0,371,375,
        5,28,0,0,372,374,3,50,25,0,373,372,1,0,0,0,374,377,1,0,0,0,375,373,
        1,0,0,0,375,376,1,0,0,0,376,378,1,0,0,0,377,375,1,0,0,0,378,382,
        5,29,0,0,379,380,5,12,0,0,380,382,3,50,25,0,381,370,1,0,0,0,381,
        379,1,0,0,0,382,49,1,0,0,0,383,384,5,51,0,0,384,386,5,28,0,0,385,
        387,3,54,27,0,386,385,1,0,0,0,386,387,1,0,0,0,387,390,1,0,0,0,388,
        389,5,13,0,0,389,391,3,54,27,0,390,388,1,0,0,0,390,391,1,0,0,0,391,
        394,1,0,0,0,392,393,5,14,0,0,393,395,3,52,26,0,394,392,1,0,0,0,394,
        395,1,0,0,0,395,402,1,0,0,0,396,398,5,21,0,0,397,399,5,51,0,0,398,
        397,1,0,0,0,399,400,1,0,0,0,400,398,1,0,0,0,400,401,1,0,0,0,401,
        403,1,0,0,0,402,396,1,0,0,0,402,403,1,0,0,0,403,404,1,0,0,0,404,
        430,5,29,0,0,405,406,5,51,0,0,406,408,5,35,0,0,407,409,3,54,27,0,
        408,407,1,0,0,0,408,409,1,0,0,0,409,413,1,0,0,0,410,411,5,13,0,0,
        411,412,5,46,0,0,412,414,3,54,27,0,413,410,1,0,0,0,413,414,1,0,0,
        0,414,418,1,0,0,0,415,416,5,14,0,0,416,417,5,46,0,0,417,419,3,52,
        26,0,418,415,1,0,0,0,418,419,1,0,0,0,419,427,1,0,0,0,420,421,5,21,
        0,0,421,423,5,46,0,0,422,424,5,51,0,0,423,422,1,0,0,0,424,425,1,
        0,0,0,425,423,1,0,0,0,425,426,1,0,0,0,426,428,1,0,0,0,427,420,1,
        0,0,0,427,428,1,0,0,0,428,430,1,0,0,0,429,383,1,0,0,0,429,405,1,
        0,0,0,430,51,1,0,0,0,431,432,6,26,-1,0,432,433,5,51,0,0,433,434,
        7,1,0,0,434,440,5,51,0,0,435,436,5,26,0,0,436,437,3,52,26,0,437,
        438,5,27,0,0,438,440,1,0,0,0,439,431,1,0,0,0,439,435,1,0,0,0,440,
        446,1,0,0,0,441,442,10,2,0,0,442,443,7,2,0,0,443,445,3,52,26,3,444,
        441,1,0,0,0,445,448,1,0,0,0,446,444,1,0,0,0,446,447,1,0,0,0,447,
        53,1,0,0,0,448,446,1,0,0,0,449,450,6,27,-1,0,450,451,5,42,0,0,451,
        474,3,54,27,10,452,453,5,26,0,0,453,454,3,54,27,0,454,455,5,27,0,
        0,455,474,1,0,0,0,456,457,5,51,0,0,457,466,5,26,0,0,458,463,3,54,
        27,0,459,460,5,33,0,0,460,462,3,54,27,0,461,459,1,0,0,0,462,465,
        1,0,0,0,463,461,1,0,0,0,463,464,1,0,0,0,464,467,1,0,0,0,465,463,
        1,0,0,0,466,458,1,0,0,0,466,467,1,0,0,0,467,468,1,0,0,0,468,474,
        5,27,0,0,469,474,7,3,0,0,470,474,7,0,0,0,471,474,5,23,0,0,472,474,
        5,51,0,0,473,449,1,0,0,0,473,452,1,0,0,0,473,456,1,0,0,0,473,469,
        1,0,0,0,473,470,1,0,0,0,473,471,1,0,0,0,473,472,1,0,0,0,474,489,
        1,0,0,0,475,476,10,11,0,0,476,477,7,4,0,0,477,488,3,54,27,12,478,
        479,10,9,0,0,479,480,5,43,0,0,480,488,3,54,27,10,481,482,10,8,0,
        0,482,483,5,44,0,0,483,488,3,54,27,9,484,485,10,7,0,0,485,486,5,
        45,0,0,486,488,3,54,27,8,487,475,1,0,0,0,487,478,1,0,0,0,487,481,
        1,0,0,0,487,484,1,0,0,0,488,491,1,0,0,0,489,487,1,0,0,0,489,490,
        1,0,0,0,490,55,1,0,0,0,491,489,1,0,0,0,492,493,5,51,0,0,493,494,
        5,46,0,0,494,495,3,54,27,0,495,496,5,32,0,0,496,57,1,0,0,0,73,60,
        62,70,76,87,89,99,105,116,122,131,134,139,150,156,164,173,184,190,
        198,201,204,207,211,214,217,226,232,243,249,256,263,267,278,283,
        291,293,298,303,308,313,317,321,324,331,337,342,347,352,357,361,
        365,368,375,381,386,390,394,400,402,408,413,418,425,427,429,439,
        446,463,466,473,487,489
    ]

class SkillsParser ( Parser ):

    grammarFileName = "Skills.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'type'", "'skillset'", "'data'", "'function'", 
                     "'initial'", "'->'", "'resource'", "'extern'", "'skill'", 
                     "'input'", "'precondition'", "'result'", "'duration'", 
                     "'post'", "'effect'", "'success'", "'failure'", "'invariant'", 
                     "'violation'", "'interruption'", "'apply'", "'progress'", 
                     "'inf'", "'true'", "'false'", "'('", "')'", "'{'", 
                     "'}'", "'['", "']'", "';'", "','", "'.'", "':'", "'>'", 
                     "'>='", "'<'", "'<='", "'=='", "'!='", "'!'", "'&&'", 
                     "'||'", "'=>'", "'='", "'not'", "'or'" ]

    symbolicNames = [ "<INVALID>", "TYPE", "SKILLSET", "DATA", "FUNCTION", 
                      "INITIAL", "ARROW", "RESOURCE", "EXTERN", "SKILL", 
                      "INPUT", "PRECONDITION", "RESULT", "DURATION", "POST", 
                      "EFFECT", "SUCCESS", "FAILURE", "INVARIANT", "VIOLATION", 
                      "INTERRUPTION", "APPLY", "PROGRESS", "INF", "TRUE", 
                      "FALSE", "LPAREN", "RPAREN", "LBRACE", "RBRACE", "LBRACK", 
                      "RBRACK", "SEMI", "COMMA", "DOT", "COLON", "GT", "GE", 
                      "LT", "LE", "EQ", "NE", "NOT", "AND", "OR", "IMPLIES", 
                      "ASSIGN", "NOT_USE", "OR_USE", "INTEGER", "FLOATING", 
                      "IDENTIFIER", "WS", "COMMENT", "LINE_COMMENT" ]

    RULE_model = 0
    RULE_typeBlock = 1
    RULE_typeDef = 2
    RULE_skillset = 3
    RULE_dataBlock = 4
    RULE_dataDef = 5
    RULE_functionBlock = 6
    RULE_functionDef = 7
    RULE_parameterDef = 8
    RULE_resourceBlock = 9
    RULE_resourceDef = 10
    RULE_resourceInitial = 11
    RULE_resourceArc = 12
    RULE_skillBlock = 13
    RULE_skillDef = 14
    RULE_inputBlock = 15
    RULE_inputDef = 16
    RULE_effectBlock = 17
    RULE_effectDef = 18
    RULE_resourceEffect = 19
    RULE_preconditionBlock = 20
    RULE_preconditionDef = 21
    RULE_invariantBlock = 22
    RULE_invariantDef = 23
    RULE_resultBlock = 24
    RULE_resultDef = 25
    RULE_resourceExpression = 26
    RULE_expression = 27
    RULE_statement = 28

    ruleNames =  [ "model", "typeBlock", "typeDef", "skillset", "dataBlock", 
                   "dataDef", "functionBlock", "functionDef", "parameterDef", 
                   "resourceBlock", "resourceDef", "resourceInitial", "resourceArc", 
                   "skillBlock", "skillDef", "inputBlock", "inputDef", "effectBlock", 
                   "effectDef", "resourceEffect", "preconditionBlock", "preconditionDef", 
                   "invariantBlock", "invariantDef", "resultBlock", "resultDef", 
                   "resourceExpression", "expression", "statement" ]

    EOF = Token.EOF
    TYPE=1
    SKILLSET=2
    DATA=3
    FUNCTION=4
    INITIAL=5
    ARROW=6
    RESOURCE=7
    EXTERN=8
    SKILL=9
    INPUT=10
    PRECONDITION=11
    RESULT=12
    DURATION=13
    POST=14
    EFFECT=15
    SUCCESS=16
    FAILURE=17
    INVARIANT=18
    VIOLATION=19
    INTERRUPTION=20
    APPLY=21
    PROGRESS=22
    INF=23
    TRUE=24
    FALSE=25
    LPAREN=26
    RPAREN=27
    LBRACE=28
    RBRACE=29
    LBRACK=30
    RBRACK=31
    SEMI=32
    COMMA=33
    DOT=34
    COLON=35
    GT=36
    GE=37
    LT=38
    LE=39
    EQ=40
    NE=41
    NOT=42
    AND=43
    OR=44
    IMPLIES=45
    ASSIGN=46
    NOT_USE=47
    OR_USE=48
    INTEGER=49
    FLOATING=50
    IDENTIFIER=51
    WS=52
    COMMENT=53
    LINE_COMMENT=54

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.13.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ModelContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._typeBlock = None # TypeBlockContext
            self.types = list() # of TypeBlockContexts
            self._skillset = None # SkillsetContext
            self.skillsets = list() # of SkillsetContexts

        def typeBlock(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.TypeBlockContext)
            else:
                return self.getTypedRuleContext(SkillsParser.TypeBlockContext,i)


        def skillset(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.SkillsetContext)
            else:
                return self.getTypedRuleContext(SkillsParser.SkillsetContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_model

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterModel" ):
                listener.enterModel(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitModel" ):
                listener.exitModel(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitModel" ):
                return visitor.visitModel(self)
            else:
                return visitor.visitChildren(self)




    def model(self):

        localctx = SkillsParser.ModelContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_model)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==1 or _la==2:
                self.state = 60
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [1]:
                    self.state = 58
                    localctx._typeBlock = self.typeBlock()
                    localctx.types.append(localctx._typeBlock)
                    pass
                elif token in [2]:
                    self.state = 59
                    localctx._skillset = self.skillset()
                    localctx.skillsets.append(localctx._skillset)
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 64
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._typeDef = None # TypeDefContext
            self.types = list() # of TypeDefContexts

        def TYPE(self):
            return self.getToken(SkillsParser.TYPE, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def typeDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.TypeDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.TypeDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_typeBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeBlock" ):
                listener.enterTypeBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeBlock" ):
                listener.exitTypeBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTypeBlock" ):
                return visitor.visitTypeBlock(self)
            else:
                return visitor.visitChildren(self)




    def typeBlock(self):

        localctx = SkillsParser.TypeBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_typeBlock)
        self._la = 0 # Token type
        try:
            self.state = 76
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 65
                self.match(SkillsParser.TYPE)
                self.state = 66
                self.match(SkillsParser.LBRACE)
                self.state = 70
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 67
                    localctx._typeDef = self.typeDef()
                    localctx.types.append(localctx._typeDef)
                    self.state = 72
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 73
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 74
                self.match(SkillsParser.TYPE)
                self.state = 75
                localctx._typeDef = self.typeDef()
                localctx.types.append(localctx._typeDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypeDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token

        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_typeDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTypeDef" ):
                listener.enterTypeDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTypeDef" ):
                listener.exitTypeDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTypeDef" ):
                return visitor.visitTypeDef(self)
            else:
                return visitor.visitChildren(self)




    def typeDef(self):

        localctx = SkillsParser.TypeDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_typeDef)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 78
            localctx.name = self.match(SkillsParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SkillsetContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self._dataBlock = None # DataBlockContext
            self.data = list() # of DataBlockContexts
            self._functionBlock = None # FunctionBlockContext
            self.functions = list() # of FunctionBlockContexts
            self._resourceBlock = None # ResourceBlockContext
            self.resources = list() # of ResourceBlockContexts
            self._skillBlock = None # SkillBlockContext
            self.skills = list() # of SkillBlockContexts

        def SKILLSET(self):
            return self.getToken(SkillsParser.SKILLSET, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)

        def dataBlock(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.DataBlockContext)
            else:
                return self.getTypedRuleContext(SkillsParser.DataBlockContext,i)


        def functionBlock(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.FunctionBlockContext)
            else:
                return self.getTypedRuleContext(SkillsParser.FunctionBlockContext,i)


        def resourceBlock(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ResourceBlockContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ResourceBlockContext,i)


        def skillBlock(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.SkillBlockContext)
            else:
                return self.getTypedRuleContext(SkillsParser.SkillBlockContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_skillset

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSkillset" ):
                listener.enterSkillset(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSkillset" ):
                listener.exitSkillset(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSkillset" ):
                return visitor.visitSkillset(self)
            else:
                return visitor.visitChildren(self)




    def skillset(self):

        localctx = SkillsParser.SkillsetContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_skillset)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 80
            self.match(SkillsParser.SKILLSET)
            self.state = 81
            localctx.name = self.match(SkillsParser.IDENTIFIER)
            self.state = 82
            self.match(SkillsParser.LBRACE)
            self.state = 89
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & 664) != 0):
                self.state = 87
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [3]:
                    self.state = 83
                    localctx._dataBlock = self.dataBlock()
                    localctx.data.append(localctx._dataBlock)
                    pass
                elif token in [4]:
                    self.state = 84
                    localctx._functionBlock = self.functionBlock()
                    localctx.functions.append(localctx._functionBlock)
                    pass
                elif token in [7]:
                    self.state = 85
                    localctx._resourceBlock = self.resourceBlock()
                    localctx.resources.append(localctx._resourceBlock)
                    pass
                elif token in [9]:
                    self.state = 86
                    localctx._skillBlock = self.skillBlock()
                    localctx.skills.append(localctx._skillBlock)
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 91
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 92
            self.match(SkillsParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DataBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._dataDef = None # DataDefContext
            self.data = list() # of DataDefContexts

        def DATA(self):
            return self.getToken(SkillsParser.DATA, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def dataDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.DataDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.DataDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_dataBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDataBlock" ):
                listener.enterDataBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDataBlock" ):
                listener.exitDataBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDataBlock" ):
                return visitor.visitDataBlock(self)
            else:
                return visitor.visitChildren(self)




    def dataBlock(self):

        localctx = SkillsParser.DataBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_dataBlock)
        self._la = 0 # Token type
        try:
            self.state = 105
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 94
                self.match(SkillsParser.DATA)
                self.state = 95
                self.match(SkillsParser.LBRACE)
                self.state = 99
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 96
                    localctx._dataDef = self.dataDef()
                    localctx.data.append(localctx._dataDef)
                    self.state = 101
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 102
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 103
                self.match(SkillsParser.DATA)
                self.state = 104
                localctx._dataDef = self.dataDef()
                localctx.data.append(localctx._dataDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DataDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.dataType = None # Token

        def COLON(self):
            return self.getToken(SkillsParser.COLON, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def getRuleIndex(self):
            return SkillsParser.RULE_dataDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDataDef" ):
                listener.enterDataDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDataDef" ):
                listener.exitDataDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDataDef" ):
                return visitor.visitDataDef(self)
            else:
                return visitor.visitChildren(self)




    def dataDef(self):

        localctx = SkillsParser.DataDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_dataDef)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 107
            localctx.name = self.match(SkillsParser.IDENTIFIER)
            self.state = 108
            self.match(SkillsParser.COLON)
            self.state = 109
            localctx.dataType = self.match(SkillsParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._functionDef = None # FunctionDefContext
            self.functions = list() # of FunctionDefContexts

        def FUNCTION(self):
            return self.getToken(SkillsParser.FUNCTION, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def functionDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.FunctionDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.FunctionDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_functionBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionBlock" ):
                listener.enterFunctionBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionBlock" ):
                listener.exitFunctionBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctionBlock" ):
                return visitor.visitFunctionBlock(self)
            else:
                return visitor.visitChildren(self)




    def functionBlock(self):

        localctx = SkillsParser.FunctionBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_functionBlock)
        self._la = 0 # Token type
        try:
            self.state = 122
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 111
                self.match(SkillsParser.FUNCTION)
                self.state = 112
                self.match(SkillsParser.LBRACE)
                self.state = 116
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 113
                    localctx._functionDef = self.functionDef()
                    localctx.functions.append(localctx._functionDef)
                    self.state = 118
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 119
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 120
                self.match(SkillsParser.FUNCTION)
                self.state = 121
                localctx._functionDef = self.functionDef()
                localctx.functions.append(localctx._functionDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self._parameterDef = None # ParameterDefContext
            self.params = list() # of ParameterDefContexts
            self.funType = None # Token

        def LPAREN(self):
            return self.getToken(SkillsParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(SkillsParser.RPAREN, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def COLON(self):
            return self.getToken(SkillsParser.COLON, 0)

        def parameterDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ParameterDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ParameterDefContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.COMMA)
            else:
                return self.getToken(SkillsParser.COMMA, i)

        def getRuleIndex(self):
            return SkillsParser.RULE_functionDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionDef" ):
                listener.enterFunctionDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionDef" ):
                listener.exitFunctionDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctionDef" ):
                return visitor.visitFunctionDef(self)
            else:
                return visitor.visitChildren(self)




    def functionDef(self):

        localctx = SkillsParser.FunctionDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_functionDef)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 124
            localctx.name = self.match(SkillsParser.IDENTIFIER)
            self.state = 125
            self.match(SkillsParser.LPAREN)
            self.state = 134
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==51:
                self.state = 126
                localctx._parameterDef = self.parameterDef()
                localctx.params.append(localctx._parameterDef)
                self.state = 131
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==33:
                    self.state = 127
                    self.match(SkillsParser.COMMA)
                    self.state = 128
                    localctx._parameterDef = self.parameterDef()
                    localctx.params.append(localctx._parameterDef)
                    self.state = 133
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 136
            self.match(SkillsParser.RPAREN)
            self.state = 139
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==35:
                self.state = 137
                self.match(SkillsParser.COLON)
                self.state = 138
                localctx.funType = self.match(SkillsParser.IDENTIFIER)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.paramType = None # Token

        def COLON(self):
            return self.getToken(SkillsParser.COLON, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def getRuleIndex(self):
            return SkillsParser.RULE_parameterDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParameterDef" ):
                listener.enterParameterDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParameterDef" ):
                listener.exitParameterDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameterDef" ):
                return visitor.visitParameterDef(self)
            else:
                return visitor.visitChildren(self)




    def parameterDef(self):

        localctx = SkillsParser.ParameterDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_parameterDef)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 141
            localctx.name = self.match(SkillsParser.IDENTIFIER)
            self.state = 142
            self.match(SkillsParser.COLON)
            self.state = 143
            localctx.paramType = self.match(SkillsParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ResourceBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._resourceDef = None # ResourceDefContext
            self.resources = list() # of ResourceDefContexts

        def RESOURCE(self):
            return self.getToken(SkillsParser.RESOURCE, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def resourceDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ResourceDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ResourceDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_resourceBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterResourceBlock" ):
                listener.enterResourceBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitResourceBlock" ):
                listener.exitResourceBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitResourceBlock" ):
                return visitor.visitResourceBlock(self)
            else:
                return visitor.visitChildren(self)




    def resourceBlock(self):

        localctx = SkillsParser.ResourceBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_resourceBlock)
        self._la = 0 # Token type
        try:
            self.state = 156
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 145
                self.match(SkillsParser.RESOURCE)
                self.state = 146
                self.match(SkillsParser.LBRACE)
                self.state = 150
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 147
                    localctx._resourceDef = self.resourceDef()
                    localctx.resources.append(localctx._resourceDef)
                    self.state = 152
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 153
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 154
                self.match(SkillsParser.RESOURCE)
                self.state = 155
                localctx._resourceDef = self.resourceDef()
                localctx.resources.append(localctx._resourceDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ResourceDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.initial = None # ResourceInitialContext
            self._resourceArc = None # ResourceArcContext
            self.arcs = list() # of ResourceArcContexts

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)

        def resourceInitial(self):
            return self.getTypedRuleContext(SkillsParser.ResourceInitialContext,0)


        def resourceArc(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ResourceArcContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ResourceArcContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_resourceDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterResourceDef" ):
                listener.enterResourceDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitResourceDef" ):
                listener.exitResourceDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitResourceDef" ):
                return visitor.visitResourceDef(self)
            else:
                return visitor.visitChildren(self)




    def resourceDef(self):

        localctx = SkillsParser.ResourceDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_resourceDef)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 158
            localctx.name = self.match(SkillsParser.IDENTIFIER)
            self.state = 159
            self.match(SkillsParser.LBRACE)
            self.state = 160
            localctx.initial = self.resourceInitial()
            self.state = 164
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==8 or _la==51:
                self.state = 161
                localctx._resourceArc = self.resourceArc()
                localctx.arcs.append(localctx._resourceArc)
                self.state = 166
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 167
            self.match(SkillsParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ResourceInitialContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token

        def INITIAL(self):
            return self.getToken(SkillsParser.INITIAL, 0)

        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_resourceInitial

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterResourceInitial" ):
                listener.enterResourceInitial(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitResourceInitial" ):
                listener.exitResourceInitial(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitResourceInitial" ):
                return visitor.visitResourceInitial(self)
            else:
                return visitor.visitChildren(self)




    def resourceInitial(self):

        localctx = SkillsParser.ResourceInitialContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_resourceInitial)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 169
            self.match(SkillsParser.INITIAL)
            self.state = 170
            localctx.name = self.match(SkillsParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ResourceArcContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.is_extern = None # Token
            self.src = None # Token
            self.dst = None # Token

        def ARROW(self):
            return self.getToken(SkillsParser.ARROW, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def EXTERN(self):
            return self.getToken(SkillsParser.EXTERN, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_resourceArc

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterResourceArc" ):
                listener.enterResourceArc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitResourceArc" ):
                listener.exitResourceArc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitResourceArc" ):
                return visitor.visitResourceArc(self)
            else:
                return visitor.visitChildren(self)




    def resourceArc(self):

        localctx = SkillsParser.ResourceArcContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_resourceArc)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 173
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==8:
                self.state = 172
                localctx.is_extern = self.match(SkillsParser.EXTERN)


            self.state = 175
            localctx.src = self.match(SkillsParser.IDENTIFIER)
            self.state = 176
            self.match(SkillsParser.ARROW)
            self.state = 177
            localctx.dst = self.match(SkillsParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SkillBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._skillDef = None # SkillDefContext
            self.skills = list() # of SkillDefContexts

        def SKILL(self):
            return self.getToken(SkillsParser.SKILL, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def skillDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.SkillDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.SkillDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_skillBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSkillBlock" ):
                listener.enterSkillBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSkillBlock" ):
                listener.exitSkillBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSkillBlock" ):
                return visitor.visitSkillBlock(self)
            else:
                return visitor.visitChildren(self)




    def skillBlock(self):

        localctx = SkillsParser.SkillBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_skillBlock)
        self._la = 0 # Token type
        try:
            self.state = 190
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 179
                self.match(SkillsParser.SKILL)
                self.state = 180
                self.match(SkillsParser.LBRACE)
                self.state = 184
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 181
                    localctx._skillDef = self.skillDef()
                    localctx.skills.append(localctx._skillDef)
                    self.state = 186
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 187
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 188
                self.match(SkillsParser.SKILL)
                self.state = 189
                localctx._skillDef = self.skillDef()
                localctx.skills.append(localctx._skillDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SkillDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.progress = None # Token
            self.inputs = None # InputBlockContext
            self.effects = None # EffectBlockContext
            self.preconditions = None # PreconditionBlockContext
            self.invariants = None # InvariantBlockContext
            self.interrupt_effect = None # Token
            self.results = None # ResultBlockContext

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def PROGRESS(self):
            return self.getToken(SkillsParser.PROGRESS, 0)

        def ASSIGN(self):
            return self.getToken(SkillsParser.ASSIGN, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def INTEGER(self):
            return self.getToken(SkillsParser.INTEGER, 0)

        def FLOATING(self):
            return self.getToken(SkillsParser.FLOATING, 0)

        def INTERRUPTION(self):
            return self.getToken(SkillsParser.INTERRUPTION, 0)

        def inputBlock(self):
            return self.getTypedRuleContext(SkillsParser.InputBlockContext,0)


        def effectBlock(self):
            return self.getTypedRuleContext(SkillsParser.EffectBlockContext,0)


        def preconditionBlock(self):
            return self.getTypedRuleContext(SkillsParser.PreconditionBlockContext,0)


        def invariantBlock(self):
            return self.getTypedRuleContext(SkillsParser.InvariantBlockContext,0)


        def resultBlock(self):
            return self.getTypedRuleContext(SkillsParser.ResultBlockContext,0)


        def EFFECT(self):
            return self.getToken(SkillsParser.EFFECT, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_skillDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSkillDef" ):
                listener.enterSkillDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSkillDef" ):
                listener.exitSkillDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSkillDef" ):
                return visitor.visitSkillDef(self)
            else:
                return visitor.visitChildren(self)




    def skillDef(self):

        localctx = SkillsParser.SkillDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_skillDef)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 192
            localctx.name = self.match(SkillsParser.IDENTIFIER)
            self.state = 193
            self.match(SkillsParser.LBRACE)
            self.state = 194
            self.match(SkillsParser.PROGRESS)
            self.state = 195
            self.match(SkillsParser.ASSIGN)
            self.state = 196
            localctx.progress = self._input.LT(1)
            _la = self._input.LA(1)
            if not(_la==49 or _la==50):
                localctx.progress = self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 198
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==10:
                self.state = 197
                localctx.inputs = self.inputBlock()


            self.state = 201
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==15:
                self.state = 200
                localctx.effects = self.effectBlock()


            self.state = 204
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==11:
                self.state = 203
                localctx.preconditions = self.preconditionBlock()


            self.state = 207
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==18:
                self.state = 206
                localctx.invariants = self.invariantBlock()


            self.state = 214
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==20:
                self.state = 209
                self.match(SkillsParser.INTERRUPTION)
                self.state = 211
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==15:
                    self.state = 210
                    self.match(SkillsParser.EFFECT)


                self.state = 213
                localctx.interrupt_effect = self.match(SkillsParser.IDENTIFIER)


            self.state = 217
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==12:
                self.state = 216
                localctx.results = self.resultBlock()


            self.state = 219
            self.match(SkillsParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InputBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._inputDef = None # InputDefContext
            self.inputs = list() # of InputDefContexts

        def INPUT(self):
            return self.getToken(SkillsParser.INPUT, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def inputDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.InputDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.InputDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_inputBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInputBlock" ):
                listener.enterInputBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInputBlock" ):
                listener.exitInputBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInputBlock" ):
                return visitor.visitInputBlock(self)
            else:
                return visitor.visitChildren(self)




    def inputBlock(self):

        localctx = SkillsParser.InputBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_inputBlock)
        self._la = 0 # Token type
        try:
            self.state = 232
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,27,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 221
                self.match(SkillsParser.INPUT)
                self.state = 222
                self.match(SkillsParser.LBRACE)
                self.state = 226
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 223
                    localctx._inputDef = self.inputDef()
                    localctx.inputs.append(localctx._inputDef)
                    self.state = 228
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 229
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 230
                self.match(SkillsParser.INPUT)
                self.state = 231
                localctx._inputDef = self.inputDef()
                localctx.inputs.append(localctx._inputDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InputDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.inputType = None # Token

        def COLON(self):
            return self.getToken(SkillsParser.COLON, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def getRuleIndex(self):
            return SkillsParser.RULE_inputDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInputDef" ):
                listener.enterInputDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInputDef" ):
                listener.exitInputDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInputDef" ):
                return visitor.visitInputDef(self)
            else:
                return visitor.visitChildren(self)




    def inputDef(self):

        localctx = SkillsParser.InputDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_inputDef)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 234
            localctx.name = self.match(SkillsParser.IDENTIFIER)
            self.state = 235
            self.match(SkillsParser.COLON)
            self.state = 236
            localctx.inputType = self.match(SkillsParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EffectBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._effectDef = None # EffectDefContext
            self.effects = list() # of EffectDefContexts

        def EFFECT(self):
            return self.getToken(SkillsParser.EFFECT, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def effectDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.EffectDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.EffectDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_effectBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEffectBlock" ):
                listener.enterEffectBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEffectBlock" ):
                listener.exitEffectBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEffectBlock" ):
                return visitor.visitEffectBlock(self)
            else:
                return visitor.visitChildren(self)




    def effectBlock(self):

        localctx = SkillsParser.EffectBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_effectBlock)
        self._la = 0 # Token type
        try:
            self.state = 249
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,29,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 238
                self.match(SkillsParser.EFFECT)
                self.state = 239
                self.match(SkillsParser.LBRACE)
                self.state = 243
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 240
                    localctx._effectDef = self.effectDef()
                    localctx.effects.append(localctx._effectDef)
                    self.state = 245
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 246
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 247
                self.match(SkillsParser.EFFECT)
                self.state = 248
                localctx._effectDef = self.effectDef()
                localctx.effects.append(localctx._effectDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EffectDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self._resourceEffect = None # ResourceEffectContext
            self.effects = list() # of ResourceEffectContexts

        def COLON(self):
            return self.getToken(SkillsParser.COLON, 0)

        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)

        def resourceEffect(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ResourceEffectContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ResourceEffectContext,i)


        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_effectDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEffectDef" ):
                listener.enterEffectDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEffectDef" ):
                listener.exitEffectDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEffectDef" ):
                return visitor.visitEffectDef(self)
            else:
                return visitor.visitChildren(self)




    def effectDef(self):

        localctx = SkillsParser.EffectDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_effectDef)
        self._la = 0 # Token type
        try:
            self.state = 267
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,32,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 251
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 252
                self.match(SkillsParser.COLON)
                self.state = 254 
                self._errHandler.sync(self)
                _alt = 1
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt == 1:
                        self.state = 253
                        localctx._resourceEffect = self.resourceEffect()
                        localctx.effects.append(localctx._resourceEffect)

                    else:
                        raise NoViableAltException(self)
                    self.state = 256 
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,30,self._ctx)

                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 258
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 259
                self.match(SkillsParser.LBRACE)
                self.state = 263
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 260
                    localctx._resourceEffect = self.resourceEffect()
                    localctx.effects.append(localctx._resourceEffect)
                    self.state = 265
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 266
                self.match(SkillsParser.RBRACE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ResourceEffectContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.state_ = None # Token

        def ARROW(self):
            return self.getToken(SkillsParser.ARROW, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def getRuleIndex(self):
            return SkillsParser.RULE_resourceEffect

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterResourceEffect" ):
                listener.enterResourceEffect(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitResourceEffect" ):
                listener.exitResourceEffect(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitResourceEffect" ):
                return visitor.visitResourceEffect(self)
            else:
                return visitor.visitChildren(self)




    def resourceEffect(self):

        localctx = SkillsParser.ResourceEffectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_resourceEffect)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 269
            localctx.name = self.match(SkillsParser.IDENTIFIER)
            self.state = 270
            self.match(SkillsParser.ARROW)
            self.state = 271
            localctx.state_ = self.match(SkillsParser.IDENTIFIER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PreconditionBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._preconditionDef = None # PreconditionDefContext
            self.preconds = list() # of PreconditionDefContexts
            self.success = None # Token

        def PRECONDITION(self):
            return self.getToken(SkillsParser.PRECONDITION, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def SUCCESS(self):
            return self.getToken(SkillsParser.SUCCESS, 0)

        def preconditionDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.PreconditionDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.PreconditionDefContext,i)


        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)

        def ASSIGN(self):
            return self.getToken(SkillsParser.ASSIGN, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_preconditionBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPreconditionBlock" ):
                listener.enterPreconditionBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPreconditionBlock" ):
                listener.exitPreconditionBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPreconditionBlock" ):
                return visitor.visitPreconditionBlock(self)
            else:
                return visitor.visitChildren(self)




    def preconditionBlock(self):

        localctx = SkillsParser.PreconditionBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_preconditionBlock)
        self._la = 0 # Token type
        try:
            self.state = 293
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,36,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 273
                self.match(SkillsParser.PRECONDITION)
                self.state = 274
                self.match(SkillsParser.LBRACE)
                self.state = 278
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 275
                    localctx._preconditionDef = self.preconditionDef()
                    localctx.preconds.append(localctx._preconditionDef)
                    self.state = 280
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 283
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==16:
                    self.state = 281
                    self.match(SkillsParser.SUCCESS)
                    self.state = 282
                    localctx.success = self.match(SkillsParser.IDENTIFIER)


                self.state = 285
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 286
                self.match(SkillsParser.PRECONDITION)
                self.state = 287
                localctx._preconditionDef = self.preconditionDef()
                localctx.preconds.append(localctx._preconditionDef)
                self.state = 291
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==16:
                    self.state = 288
                    self.match(SkillsParser.SUCCESS)
                    self.state = 289
                    self.match(SkillsParser.ASSIGN)
                    self.state = 290
                    localctx.success = self.match(SkillsParser.IDENTIFIER)


                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PreconditionDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.expr = None # ExpressionContext
            self.res_expr = None # ResourceExpressionContext
            self.failure = None # Token

        def COLON(self):
            return self.getToken(SkillsParser.COLON, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def RESOURCE(self):
            return self.getToken(SkillsParser.RESOURCE, 0)

        def ASSIGN(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.ASSIGN)
            else:
                return self.getToken(SkillsParser.ASSIGN, i)

        def FAILURE(self):
            return self.getToken(SkillsParser.FAILURE, 0)

        def expression(self):
            return self.getTypedRuleContext(SkillsParser.ExpressionContext,0)


        def resourceExpression(self):
            return self.getTypedRuleContext(SkillsParser.ResourceExpressionContext,0)


        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_preconditionDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPreconditionDef" ):
                listener.enterPreconditionDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPreconditionDef" ):
                listener.exitPreconditionDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPreconditionDef" ):
                return visitor.visitPreconditionDef(self)
            else:
                return visitor.visitChildren(self)




    def preconditionDef(self):

        localctx = SkillsParser.PreconditionDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_preconditionDef)
        self._la = 0 # Token type
        try:
            self.state = 324
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,43,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 295
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 296
                self.match(SkillsParser.COLON)
                self.state = 298
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,37,self._ctx)
                if la_ == 1:
                    self.state = 297
                    localctx.expr = self.expression(0)


                self.state = 303
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==7:
                    self.state = 300
                    self.match(SkillsParser.RESOURCE)
                    self.state = 301
                    self.match(SkillsParser.ASSIGN)
                    self.state = 302
                    localctx.res_expr = self.resourceExpression(0)


                self.state = 308
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==17:
                    self.state = 305
                    self.match(SkillsParser.FAILURE)
                    self.state = 306
                    self.match(SkillsParser.ASSIGN)
                    self.state = 307
                    localctx.failure = self.match(SkillsParser.IDENTIFIER)


                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 310
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 311
                self.match(SkillsParser.LBRACE)
                self.state = 313
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & 3945047846289408) != 0):
                    self.state = 312
                    localctx.expr = self.expression(0)


                self.state = 317
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==7:
                    self.state = 315
                    self.match(SkillsParser.RESOURCE)
                    self.state = 316
                    localctx.res_expr = self.resourceExpression(0)


                self.state = 321
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==17:
                    self.state = 319
                    self.match(SkillsParser.FAILURE)
                    self.state = 320
                    localctx.failure = self.match(SkillsParser.IDENTIFIER)


                self.state = 323
                self.match(SkillsParser.RBRACE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InvariantBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._invariantDef = None # InvariantDefContext
            self.invariants = list() # of InvariantDefContexts

        def INVARIANT(self):
            return self.getToken(SkillsParser.INVARIANT, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def invariantDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.InvariantDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.InvariantDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_invariantBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInvariantBlock" ):
                listener.enterInvariantBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInvariantBlock" ):
                listener.exitInvariantBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInvariantBlock" ):
                return visitor.visitInvariantBlock(self)
            else:
                return visitor.visitChildren(self)




    def invariantBlock(self):

        localctx = SkillsParser.InvariantBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_invariantBlock)
        self._la = 0 # Token type
        try:
            self.state = 337
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,45,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 326
                self.match(SkillsParser.INVARIANT)
                self.state = 327
                self.match(SkillsParser.LBRACE)
                self.state = 331
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 328
                    localctx._invariantDef = self.invariantDef()
                    localctx.invariants.append(localctx._invariantDef)
                    self.state = 333
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 334
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 335
                self.match(SkillsParser.INVARIANT)
                self.state = 336
                localctx._invariantDef = self.invariantDef()
                localctx.invariants.append(localctx._invariantDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InvariantDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.expr = None # ExpressionContext
            self.res_expr = None # ResourceExpressionContext
            self.effect = None # Token

        def COLON(self):
            return self.getToken(SkillsParser.COLON, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def RESOURCE(self):
            return self.getToken(SkillsParser.RESOURCE, 0)

        def ASSIGN(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.ASSIGN)
            else:
                return self.getToken(SkillsParser.ASSIGN, i)

        def VIOLATION(self):
            return self.getToken(SkillsParser.VIOLATION, 0)

        def expression(self):
            return self.getTypedRuleContext(SkillsParser.ExpressionContext,0)


        def resourceExpression(self):
            return self.getTypedRuleContext(SkillsParser.ResourceExpressionContext,0)


        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_invariantDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInvariantDef" ):
                listener.enterInvariantDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInvariantDef" ):
                listener.exitInvariantDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInvariantDef" ):
                return visitor.visitInvariantDef(self)
            else:
                return visitor.visitChildren(self)




    def invariantDef(self):

        localctx = SkillsParser.InvariantDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_invariantDef)
        self._la = 0 # Token type
        try:
            self.state = 368
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,52,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 339
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 340
                self.match(SkillsParser.COLON)
                self.state = 342
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,46,self._ctx)
                if la_ == 1:
                    self.state = 341
                    localctx.expr = self.expression(0)


                self.state = 347
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==7:
                    self.state = 344
                    self.match(SkillsParser.RESOURCE)
                    self.state = 345
                    self.match(SkillsParser.ASSIGN)
                    self.state = 346
                    localctx.res_expr = self.resourceExpression(0)


                self.state = 352
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==19:
                    self.state = 349
                    self.match(SkillsParser.VIOLATION)
                    self.state = 350
                    self.match(SkillsParser.ASSIGN)
                    self.state = 351
                    localctx.effect = self.match(SkillsParser.IDENTIFIER)


                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 354
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 355
                self.match(SkillsParser.LBRACE)
                self.state = 357
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & 3945047846289408) != 0):
                    self.state = 356
                    localctx.expr = self.expression(0)


                self.state = 361
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==7:
                    self.state = 359
                    self.match(SkillsParser.RESOURCE)
                    self.state = 360
                    localctx.res_expr = self.resourceExpression(0)


                self.state = 365
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==19:
                    self.state = 363
                    self.match(SkillsParser.VIOLATION)
                    self.state = 364
                    localctx.effect = self.match(SkillsParser.IDENTIFIER)


                self.state = 367
                self.match(SkillsParser.RBRACE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ResultBlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._resultDef = None # ResultDefContext
            self.results = list() # of ResultDefContexts

        def RESULT(self):
            return self.getToken(SkillsParser.RESULT, 0)

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def resultDef(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ResultDefContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ResultDefContext,i)


        def getRuleIndex(self):
            return SkillsParser.RULE_resultBlock

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterResultBlock" ):
                listener.enterResultBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitResultBlock" ):
                listener.exitResultBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitResultBlock" ):
                return visitor.visitResultBlock(self)
            else:
                return visitor.visitChildren(self)




    def resultBlock(self):

        localctx = SkillsParser.ResultBlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_resultBlock)
        self._la = 0 # Token type
        try:
            self.state = 381
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,54,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 370
                self.match(SkillsParser.RESULT)
                self.state = 371
                self.match(SkillsParser.LBRACE)
                self.state = 375
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==51:
                    self.state = 372
                    localctx._resultDef = self.resultDef()
                    localctx.results.append(localctx._resultDef)
                    self.state = 377
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 378
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 379
                self.match(SkillsParser.RESULT)
                self.state = 380
                localctx._resultDef = self.resultDef()
                localctx.results.append(localctx._resultDef)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ResultDefContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.expr = None # ExpressionContext
            self.duration = None # ExpressionContext
            self.post = None # ResourceExpressionContext
            self._IDENTIFIER = None # Token
            self.effects = list() # of Tokens

        def LBRACE(self):
            return self.getToken(SkillsParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SkillsParser.RBRACE, 0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)

        def DURATION(self):
            return self.getToken(SkillsParser.DURATION, 0)

        def POST(self):
            return self.getToken(SkillsParser.POST, 0)

        def APPLY(self):
            return self.getToken(SkillsParser.APPLY, 0)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ExpressionContext,i)


        def resourceExpression(self):
            return self.getTypedRuleContext(SkillsParser.ResourceExpressionContext,0)


        def COLON(self):
            return self.getToken(SkillsParser.COLON, 0)

        def ASSIGN(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.ASSIGN)
            else:
                return self.getToken(SkillsParser.ASSIGN, i)

        def getRuleIndex(self):
            return SkillsParser.RULE_resultDef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterResultDef" ):
                listener.enterResultDef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitResultDef" ):
                listener.exitResultDef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitResultDef" ):
                return visitor.visitResultDef(self)
            else:
                return visitor.visitChildren(self)




    def resultDef(self):

        localctx = SkillsParser.ResultDefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_resultDef)
        self._la = 0 # Token type
        try:
            self.state = 429
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,65,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 383
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 384
                self.match(SkillsParser.LBRACE)
                self.state = 386
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & 3945047846289408) != 0):
                    self.state = 385
                    localctx.expr = self.expression(0)


                self.state = 390
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==13:
                    self.state = 388
                    self.match(SkillsParser.DURATION)
                    self.state = 389
                    localctx.duration = self.expression(0)


                self.state = 394
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==14:
                    self.state = 392
                    self.match(SkillsParser.POST)
                    self.state = 393
                    localctx.post = self.resourceExpression(0)


                self.state = 402
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==21:
                    self.state = 396
                    self.match(SkillsParser.APPLY)
                    self.state = 398 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while True:
                        self.state = 397
                        localctx._IDENTIFIER = self.match(SkillsParser.IDENTIFIER)
                        localctx.effects.append(localctx._IDENTIFIER)
                        self.state = 400 
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if not (_la==51):
                            break



                self.state = 404
                self.match(SkillsParser.RBRACE)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 405
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 406
                self.match(SkillsParser.COLON)
                self.state = 408
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,60,self._ctx)
                if la_ == 1:
                    self.state = 407
                    localctx.expr = self.expression(0)


                self.state = 413
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==13:
                    self.state = 410
                    self.match(SkillsParser.DURATION)
                    self.state = 411
                    self.match(SkillsParser.ASSIGN)
                    self.state = 412
                    localctx.duration = self.expression(0)


                self.state = 418
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==14:
                    self.state = 415
                    self.match(SkillsParser.POST)
                    self.state = 416
                    self.match(SkillsParser.ASSIGN)
                    self.state = 417
                    localctx.post = self.resourceExpression(0)


                self.state = 427
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==21:
                    self.state = 420
                    self.match(SkillsParser.APPLY)
                    self.state = 421
                    self.match(SkillsParser.ASSIGN)
                    self.state = 423 
                    self._errHandler.sync(self)
                    _alt = 1
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt == 1:
                            self.state = 422
                            localctx._IDENTIFIER = self.match(SkillsParser.IDENTIFIER)
                            localctx.effects.append(localctx._IDENTIFIER)

                        else:
                            raise NoViableAltException(self)
                        self.state = 425 
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,63,self._ctx)



                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ResourceExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SkillsParser.RULE_resourceExpression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class BinBoolResExpContext(ResourceExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ResourceExpressionContext
            super().__init__(parser)
            self.left = None # ResourceExpressionContext
            self.op = None # Token
            self.right = None # ResourceExpressionContext
            self.copyFrom(ctx)

        def resourceExpression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ResourceExpressionContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ResourceExpressionContext,i)

        def AND(self):
            return self.getToken(SkillsParser.AND, 0)
        def OR(self):
            return self.getToken(SkillsParser.OR, 0)
        def IMPLIES(self):
            return self.getToken(SkillsParser.IMPLIES, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBinBoolResExp" ):
                listener.enterBinBoolResExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBinBoolResExp" ):
                listener.exitBinBoolResExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBinBoolResExp" ):
                return visitor.visitBinBoolResExp(self)
            else:
                return visitor.visitChildren(self)


    class BinCompResExpContext(ResourceExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ResourceExpressionContext
            super().__init__(parser)
            self.name = None # Token
            self.op = None # Token
            self.state_ = None # Token
            self.copyFrom(ctx)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.IDENTIFIER)
            else:
                return self.getToken(SkillsParser.IDENTIFIER, i)
        def EQ(self):
            return self.getToken(SkillsParser.EQ, 0)
        def NE(self):
            return self.getToken(SkillsParser.NE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBinCompResExp" ):
                listener.enterBinCompResExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBinCompResExp" ):
                listener.exitBinCompResExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBinCompResExp" ):
                return visitor.visitBinCompResExp(self)
            else:
                return visitor.visitChildren(self)


    class ParenResExpContext(ResourceExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ResourceExpressionContext
            super().__init__(parser)
            self.expr = None # ResourceExpressionContext
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(SkillsParser.LPAREN, 0)
        def RPAREN(self):
            return self.getToken(SkillsParser.RPAREN, 0)
        def resourceExpression(self):
            return self.getTypedRuleContext(SkillsParser.ResourceExpressionContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParenResExp" ):
                listener.enterParenResExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParenResExp" ):
                listener.exitParenResExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParenResExp" ):
                return visitor.visitParenResExp(self)
            else:
                return visitor.visitChildren(self)



    def resourceExpression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = SkillsParser.ResourceExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 52
        self.enterRecursionRule(localctx, 52, self.RULE_resourceExpression, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 439
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [51]:
                localctx = SkillsParser.BinCompResExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 432
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                self.state = 433
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==40 or _la==41):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 434
                localctx.state_ = self.match(SkillsParser.IDENTIFIER)
                pass
            elif token in [26]:
                localctx = SkillsParser.ParenResExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 435
                self.match(SkillsParser.LPAREN)
                self.state = 436
                localctx.expr = self.resourceExpression(0)
                self.state = 437
                self.match(SkillsParser.RPAREN)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 446
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,67,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = SkillsParser.BinBoolResExpContext(self, SkillsParser.ResourceExpressionContext(self, _parentctx, _parentState))
                    localctx.left = _prevctx
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_resourceExpression)
                    self.state = 441
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 442
                    localctx.op = self._input.LT(1)
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 61572651155456) != 0)):
                        localctx.op = self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 443
                    localctx.right = self.resourceExpression(3) 
                self.state = 448
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,67,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class ExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return SkillsParser.RULE_expression

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class NotExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.op = None # Token
            self.expr = None # ExpressionContext
            self.copyFrom(ctx)

        def NOT(self):
            return self.getToken(SkillsParser.NOT, 0)
        def expression(self):
            return self.getTypedRuleContext(SkillsParser.ExpressionContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNotExp" ):
                listener.enterNotExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNotExp" ):
                listener.exitNotExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNotExp" ):
                return visitor.visitNotExp(self)
            else:
                return visitor.visitChildren(self)


    class AndExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.left = None # ExpressionContext
            self.op = None # Token
            self.right = None # ExpressionContext
            self.copyFrom(ctx)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ExpressionContext,i)

        def AND(self):
            return self.getToken(SkillsParser.AND, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAndExp" ):
                listener.enterAndExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAndExp" ):
                listener.exitAndExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAndExp" ):
                return visitor.visitAndExp(self)
            else:
                return visitor.visitChildren(self)


    class IdentifierContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.name = None # Token
            self.copyFrom(ctx)

        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIdentifier" ):
                listener.enterIdentifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIdentifier" ):
                listener.exitIdentifier(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIdentifier" ):
                return visitor.visitIdentifier(self)
            else:
                return visitor.visitChildren(self)


    class FunCallContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.function = None # Token
            self._expression = None # ExpressionContext
            self.params = list() # of ExpressionContexts
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(SkillsParser.LPAREN, 0)
        def RPAREN(self):
            return self.getToken(SkillsParser.RPAREN, 0)
        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)
        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ExpressionContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(SkillsParser.COMMA)
            else:
                return self.getToken(SkillsParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunCall" ):
                listener.enterFunCall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunCall" ):
                listener.exitFunCall(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunCall" ):
                return visitor.visitFunCall(self)
            else:
                return visitor.visitChildren(self)


    class NumberContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.value = None # Token
            self.copyFrom(ctx)

        def INTEGER(self):
            return self.getToken(SkillsParser.INTEGER, 0)
        def FLOATING(self):
            return self.getToken(SkillsParser.FLOATING, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber" ):
                listener.enterNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber" ):
                listener.exitNumber(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumber" ):
                return visitor.visitNumber(self)
            else:
                return visitor.visitChildren(self)


    class OrExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.left = None # ExpressionContext
            self.op = None # Token
            self.right = None # ExpressionContext
            self.copyFrom(ctx)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ExpressionContext,i)

        def OR(self):
            return self.getToken(SkillsParser.OR, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOrExp" ):
                listener.enterOrExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOrExp" ):
                listener.exitOrExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOrExp" ):
                return visitor.visitOrExp(self)
            else:
                return visitor.visitChildren(self)


    class ParenExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.expr = None # ExpressionContext
            self.copyFrom(ctx)

        def LPAREN(self):
            return self.getToken(SkillsParser.LPAREN, 0)
        def RPAREN(self):
            return self.getToken(SkillsParser.RPAREN, 0)
        def expression(self):
            return self.getTypedRuleContext(SkillsParser.ExpressionContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParenExp" ):
                listener.enterParenExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParenExp" ):
                listener.exitParenExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParenExp" ):
                return visitor.visitParenExp(self)
            else:
                return visitor.visitChildren(self)


    class BinCompExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.left = None # ExpressionContext
            self.op = None # Token
            self.right = None # ExpressionContext
            self.copyFrom(ctx)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ExpressionContext,i)

        def GT(self):
            return self.getToken(SkillsParser.GT, 0)
        def GE(self):
            return self.getToken(SkillsParser.GE, 0)
        def LT(self):
            return self.getToken(SkillsParser.LT, 0)
        def LE(self):
            return self.getToken(SkillsParser.LE, 0)
        def EQ(self):
            return self.getToken(SkillsParser.EQ, 0)
        def NE(self):
            return self.getToken(SkillsParser.NE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBinCompExp" ):
                listener.enterBinCompExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBinCompExp" ):
                listener.exitBinCompExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBinCompExp" ):
                return visitor.visitBinCompExp(self)
            else:
                return visitor.visitChildren(self)


    class BoolValueContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.value = None # Token
            self.copyFrom(ctx)

        def TRUE(self):
            return self.getToken(SkillsParser.TRUE, 0)
        def FALSE(self):
            return self.getToken(SkillsParser.FALSE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBoolValue" ):
                listener.enterBoolValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBoolValue" ):
                listener.exitBoolValue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBoolValue" ):
                return visitor.visitBoolValue(self)
            else:
                return visitor.visitChildren(self)


    class InfiniteContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def INF(self):
            return self.getToken(SkillsParser.INF, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInfinite" ):
                listener.enterInfinite(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInfinite" ):
                listener.exitInfinite(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInfinite" ):
                return visitor.visitInfinite(self)
            else:
                return visitor.visitChildren(self)


    class ImpliesExpContext(ExpressionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a SkillsParser.ExpressionContext
            super().__init__(parser)
            self.left = None # ExpressionContext
            self.op = None # Token
            self.right = None # ExpressionContext
            self.copyFrom(ctx)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkillsParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(SkillsParser.ExpressionContext,i)

        def IMPLIES(self):
            return self.getToken(SkillsParser.IMPLIES, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImpliesExp" ):
                listener.enterImpliesExp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImpliesExp" ):
                listener.exitImpliesExp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImpliesExp" ):
                return visitor.visitImpliesExp(self)
            else:
                return visitor.visitChildren(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = SkillsParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 54
        self.enterRecursionRule(localctx, 54, self.RULE_expression, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 473
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,70,self._ctx)
            if la_ == 1:
                localctx = SkillsParser.NotExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 450
                localctx.op = self.match(SkillsParser.NOT)
                self.state = 451
                localctx.expr = self.expression(10)
                pass

            elif la_ == 2:
                localctx = SkillsParser.ParenExpContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 452
                self.match(SkillsParser.LPAREN)
                self.state = 453
                localctx.expr = self.expression(0)
                self.state = 454
                self.match(SkillsParser.RPAREN)
                pass

            elif la_ == 3:
                localctx = SkillsParser.FunCallContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 456
                localctx.function = self.match(SkillsParser.IDENTIFIER)
                self.state = 457
                self.match(SkillsParser.LPAREN)
                self.state = 466
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & 3945047846289408) != 0):
                    self.state = 458
                    localctx._expression = self.expression(0)
                    localctx.params.append(localctx._expression)
                    self.state = 463
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==33:
                        self.state = 459
                        self.match(SkillsParser.COMMA)
                        self.state = 460
                        localctx._expression = self.expression(0)
                        localctx.params.append(localctx._expression)
                        self.state = 465
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)



                self.state = 468
                self.match(SkillsParser.RPAREN)
                pass

            elif la_ == 4:
                localctx = SkillsParser.BoolValueContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 469
                localctx.value = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==24 or _la==25):
                    localctx.value = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass

            elif la_ == 5:
                localctx = SkillsParser.NumberContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 470
                localctx.value = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==49 or _la==50):
                    localctx.value = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass

            elif la_ == 6:
                localctx = SkillsParser.InfiniteContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 471
                self.match(SkillsParser.INF)
                pass

            elif la_ == 7:
                localctx = SkillsParser.IdentifierContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 472
                localctx.name = self.match(SkillsParser.IDENTIFIER)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 489
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,72,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 487
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,71,self._ctx)
                    if la_ == 1:
                        localctx = SkillsParser.BinCompExpContext(self, SkillsParser.ExpressionContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 475
                        if not self.precpred(self._ctx, 11):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 11)")
                        self.state = 476
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 4329327034368) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 477
                        localctx.right = self.expression(12)
                        pass

                    elif la_ == 2:
                        localctx = SkillsParser.AndExpContext(self, SkillsParser.ExpressionContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 478
                        if not self.precpred(self._ctx, 9):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 9)")
                        self.state = 479
                        localctx.op = self.match(SkillsParser.AND)
                        self.state = 480
                        localctx.right = self.expression(10)
                        pass

                    elif la_ == 3:
                        localctx = SkillsParser.OrExpContext(self, SkillsParser.ExpressionContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 481
                        if not self.precpred(self._ctx, 8):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 8)")
                        self.state = 482
                        localctx.op = self.match(SkillsParser.OR)
                        self.state = 483
                        localctx.right = self.expression(9)
                        pass

                    elif la_ == 4:
                        localctx = SkillsParser.ImpliesExpContext(self, SkillsParser.ExpressionContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 484
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 485
                        localctx.op = self.match(SkillsParser.IMPLIES)
                        self.state = 486
                        localctx.right = self.expression(8)
                        pass

             
                self.state = 491
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,72,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENTIFIER(self):
            return self.getToken(SkillsParser.IDENTIFIER, 0)

        def ASSIGN(self):
            return self.getToken(SkillsParser.ASSIGN, 0)

        def expression(self):
            return self.getTypedRuleContext(SkillsParser.ExpressionContext,0)


        def SEMI(self):
            return self.getToken(SkillsParser.SEMI, 0)

        def getRuleIndex(self):
            return SkillsParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = SkillsParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 492
            self.match(SkillsParser.IDENTIFIER)
            self.state = 493
            self.match(SkillsParser.ASSIGN)
            self.state = 494
            self.expression(0)
            self.state = 495
            self.match(SkillsParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[26] = self.resourceExpression_sempred
        self._predicates[27] = self.expression_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def resourceExpression_sempred(self, localctx:ResourceExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 11)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 9)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 8)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 7)
         




