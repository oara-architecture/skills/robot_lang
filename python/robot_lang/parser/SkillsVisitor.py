# Generated from Skills.g4 by ANTLR 4.13.2
from antlr4 import *
if "." in __name__:
    from .SkillsParser import SkillsParser
else:
    from SkillsParser import SkillsParser

# This class defines a complete generic visitor for a parse tree produced by SkillsParser.

class SkillsVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by SkillsParser#model.
    def visitModel(self, ctx:SkillsParser.ModelContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#typeBlock.
    def visitTypeBlock(self, ctx:SkillsParser.TypeBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#typeDef.
    def visitTypeDef(self, ctx:SkillsParser.TypeDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#skillset.
    def visitSkillset(self, ctx:SkillsParser.SkillsetContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#dataBlock.
    def visitDataBlock(self, ctx:SkillsParser.DataBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#dataDef.
    def visitDataDef(self, ctx:SkillsParser.DataDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#functionBlock.
    def visitFunctionBlock(self, ctx:SkillsParser.FunctionBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#functionDef.
    def visitFunctionDef(self, ctx:SkillsParser.FunctionDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#parameterDef.
    def visitParameterDef(self, ctx:SkillsParser.ParameterDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#resourceBlock.
    def visitResourceBlock(self, ctx:SkillsParser.ResourceBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#resourceDef.
    def visitResourceDef(self, ctx:SkillsParser.ResourceDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#resourceInitial.
    def visitResourceInitial(self, ctx:SkillsParser.ResourceInitialContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#resourceArc.
    def visitResourceArc(self, ctx:SkillsParser.ResourceArcContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#skillBlock.
    def visitSkillBlock(self, ctx:SkillsParser.SkillBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#skillDef.
    def visitSkillDef(self, ctx:SkillsParser.SkillDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#inputBlock.
    def visitInputBlock(self, ctx:SkillsParser.InputBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#inputDef.
    def visitInputDef(self, ctx:SkillsParser.InputDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#effectBlock.
    def visitEffectBlock(self, ctx:SkillsParser.EffectBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#effectDef.
    def visitEffectDef(self, ctx:SkillsParser.EffectDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#resourceEffect.
    def visitResourceEffect(self, ctx:SkillsParser.ResourceEffectContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#preconditionBlock.
    def visitPreconditionBlock(self, ctx:SkillsParser.PreconditionBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#preconditionDef.
    def visitPreconditionDef(self, ctx:SkillsParser.PreconditionDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#invariantBlock.
    def visitInvariantBlock(self, ctx:SkillsParser.InvariantBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#invariantDef.
    def visitInvariantDef(self, ctx:SkillsParser.InvariantDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#resultBlock.
    def visitResultBlock(self, ctx:SkillsParser.ResultBlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#resultDef.
    def visitResultDef(self, ctx:SkillsParser.ResultDefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#BinBoolResExp.
    def visitBinBoolResExp(self, ctx:SkillsParser.BinBoolResExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#BinCompResExp.
    def visitBinCompResExp(self, ctx:SkillsParser.BinCompResExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#ParenResExp.
    def visitParenResExp(self, ctx:SkillsParser.ParenResExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#NotExp.
    def visitNotExp(self, ctx:SkillsParser.NotExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#AndExp.
    def visitAndExp(self, ctx:SkillsParser.AndExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#Identifier.
    def visitIdentifier(self, ctx:SkillsParser.IdentifierContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#FunCall.
    def visitFunCall(self, ctx:SkillsParser.FunCallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#Number.
    def visitNumber(self, ctx:SkillsParser.NumberContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#OrExp.
    def visitOrExp(self, ctx:SkillsParser.OrExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#ParenExp.
    def visitParenExp(self, ctx:SkillsParser.ParenExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#BinCompExp.
    def visitBinCompExp(self, ctx:SkillsParser.BinCompExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#BoolValue.
    def visitBoolValue(self, ctx:SkillsParser.BoolValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#Infinite.
    def visitInfinite(self, ctx:SkillsParser.InfiniteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#ImpliesExp.
    def visitImpliesExp(self, ctx:SkillsParser.ImpliesExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by SkillsParser#statement.
    def visitStatement(self, ctx:SkillsParser.StatementContext):
        return self.visitChildren(ctx)



del SkillsParser