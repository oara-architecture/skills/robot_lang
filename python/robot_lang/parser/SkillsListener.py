# Generated from Skills.g4 by ANTLR 4.13.2
from antlr4 import *
if "." in __name__:
    from .SkillsParser import SkillsParser
else:
    from SkillsParser import SkillsParser

# This class defines a complete listener for a parse tree produced by SkillsParser.
class SkillsListener(ParseTreeListener):

    # Enter a parse tree produced by SkillsParser#model.
    def enterModel(self, ctx:SkillsParser.ModelContext):
        pass

    # Exit a parse tree produced by SkillsParser#model.
    def exitModel(self, ctx:SkillsParser.ModelContext):
        pass


    # Enter a parse tree produced by SkillsParser#typeBlock.
    def enterTypeBlock(self, ctx:SkillsParser.TypeBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#typeBlock.
    def exitTypeBlock(self, ctx:SkillsParser.TypeBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#typeDef.
    def enterTypeDef(self, ctx:SkillsParser.TypeDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#typeDef.
    def exitTypeDef(self, ctx:SkillsParser.TypeDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#skillset.
    def enterSkillset(self, ctx:SkillsParser.SkillsetContext):
        pass

    # Exit a parse tree produced by SkillsParser#skillset.
    def exitSkillset(self, ctx:SkillsParser.SkillsetContext):
        pass


    # Enter a parse tree produced by SkillsParser#dataBlock.
    def enterDataBlock(self, ctx:SkillsParser.DataBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#dataBlock.
    def exitDataBlock(self, ctx:SkillsParser.DataBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#dataDef.
    def enterDataDef(self, ctx:SkillsParser.DataDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#dataDef.
    def exitDataDef(self, ctx:SkillsParser.DataDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#functionBlock.
    def enterFunctionBlock(self, ctx:SkillsParser.FunctionBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#functionBlock.
    def exitFunctionBlock(self, ctx:SkillsParser.FunctionBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#functionDef.
    def enterFunctionDef(self, ctx:SkillsParser.FunctionDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#functionDef.
    def exitFunctionDef(self, ctx:SkillsParser.FunctionDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#parameterDef.
    def enterParameterDef(self, ctx:SkillsParser.ParameterDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#parameterDef.
    def exitParameterDef(self, ctx:SkillsParser.ParameterDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#resourceBlock.
    def enterResourceBlock(self, ctx:SkillsParser.ResourceBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#resourceBlock.
    def exitResourceBlock(self, ctx:SkillsParser.ResourceBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#resourceDef.
    def enterResourceDef(self, ctx:SkillsParser.ResourceDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#resourceDef.
    def exitResourceDef(self, ctx:SkillsParser.ResourceDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#resourceInitial.
    def enterResourceInitial(self, ctx:SkillsParser.ResourceInitialContext):
        pass

    # Exit a parse tree produced by SkillsParser#resourceInitial.
    def exitResourceInitial(self, ctx:SkillsParser.ResourceInitialContext):
        pass


    # Enter a parse tree produced by SkillsParser#resourceArc.
    def enterResourceArc(self, ctx:SkillsParser.ResourceArcContext):
        pass

    # Exit a parse tree produced by SkillsParser#resourceArc.
    def exitResourceArc(self, ctx:SkillsParser.ResourceArcContext):
        pass


    # Enter a parse tree produced by SkillsParser#skillBlock.
    def enterSkillBlock(self, ctx:SkillsParser.SkillBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#skillBlock.
    def exitSkillBlock(self, ctx:SkillsParser.SkillBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#skillDef.
    def enterSkillDef(self, ctx:SkillsParser.SkillDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#skillDef.
    def exitSkillDef(self, ctx:SkillsParser.SkillDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#inputBlock.
    def enterInputBlock(self, ctx:SkillsParser.InputBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#inputBlock.
    def exitInputBlock(self, ctx:SkillsParser.InputBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#inputDef.
    def enterInputDef(self, ctx:SkillsParser.InputDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#inputDef.
    def exitInputDef(self, ctx:SkillsParser.InputDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#effectBlock.
    def enterEffectBlock(self, ctx:SkillsParser.EffectBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#effectBlock.
    def exitEffectBlock(self, ctx:SkillsParser.EffectBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#effectDef.
    def enterEffectDef(self, ctx:SkillsParser.EffectDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#effectDef.
    def exitEffectDef(self, ctx:SkillsParser.EffectDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#resourceEffect.
    def enterResourceEffect(self, ctx:SkillsParser.ResourceEffectContext):
        pass

    # Exit a parse tree produced by SkillsParser#resourceEffect.
    def exitResourceEffect(self, ctx:SkillsParser.ResourceEffectContext):
        pass


    # Enter a parse tree produced by SkillsParser#preconditionBlock.
    def enterPreconditionBlock(self, ctx:SkillsParser.PreconditionBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#preconditionBlock.
    def exitPreconditionBlock(self, ctx:SkillsParser.PreconditionBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#preconditionDef.
    def enterPreconditionDef(self, ctx:SkillsParser.PreconditionDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#preconditionDef.
    def exitPreconditionDef(self, ctx:SkillsParser.PreconditionDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#invariantBlock.
    def enterInvariantBlock(self, ctx:SkillsParser.InvariantBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#invariantBlock.
    def exitInvariantBlock(self, ctx:SkillsParser.InvariantBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#invariantDef.
    def enterInvariantDef(self, ctx:SkillsParser.InvariantDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#invariantDef.
    def exitInvariantDef(self, ctx:SkillsParser.InvariantDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#resultBlock.
    def enterResultBlock(self, ctx:SkillsParser.ResultBlockContext):
        pass

    # Exit a parse tree produced by SkillsParser#resultBlock.
    def exitResultBlock(self, ctx:SkillsParser.ResultBlockContext):
        pass


    # Enter a parse tree produced by SkillsParser#resultDef.
    def enterResultDef(self, ctx:SkillsParser.ResultDefContext):
        pass

    # Exit a parse tree produced by SkillsParser#resultDef.
    def exitResultDef(self, ctx:SkillsParser.ResultDefContext):
        pass


    # Enter a parse tree produced by SkillsParser#BinBoolResExp.
    def enterBinBoolResExp(self, ctx:SkillsParser.BinBoolResExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#BinBoolResExp.
    def exitBinBoolResExp(self, ctx:SkillsParser.BinBoolResExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#BinCompResExp.
    def enterBinCompResExp(self, ctx:SkillsParser.BinCompResExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#BinCompResExp.
    def exitBinCompResExp(self, ctx:SkillsParser.BinCompResExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#ParenResExp.
    def enterParenResExp(self, ctx:SkillsParser.ParenResExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#ParenResExp.
    def exitParenResExp(self, ctx:SkillsParser.ParenResExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#NotExp.
    def enterNotExp(self, ctx:SkillsParser.NotExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#NotExp.
    def exitNotExp(self, ctx:SkillsParser.NotExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#AndExp.
    def enterAndExp(self, ctx:SkillsParser.AndExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#AndExp.
    def exitAndExp(self, ctx:SkillsParser.AndExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#Identifier.
    def enterIdentifier(self, ctx:SkillsParser.IdentifierContext):
        pass

    # Exit a parse tree produced by SkillsParser#Identifier.
    def exitIdentifier(self, ctx:SkillsParser.IdentifierContext):
        pass


    # Enter a parse tree produced by SkillsParser#FunCall.
    def enterFunCall(self, ctx:SkillsParser.FunCallContext):
        pass

    # Exit a parse tree produced by SkillsParser#FunCall.
    def exitFunCall(self, ctx:SkillsParser.FunCallContext):
        pass


    # Enter a parse tree produced by SkillsParser#Number.
    def enterNumber(self, ctx:SkillsParser.NumberContext):
        pass

    # Exit a parse tree produced by SkillsParser#Number.
    def exitNumber(self, ctx:SkillsParser.NumberContext):
        pass


    # Enter a parse tree produced by SkillsParser#OrExp.
    def enterOrExp(self, ctx:SkillsParser.OrExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#OrExp.
    def exitOrExp(self, ctx:SkillsParser.OrExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#ParenExp.
    def enterParenExp(self, ctx:SkillsParser.ParenExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#ParenExp.
    def exitParenExp(self, ctx:SkillsParser.ParenExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#BinCompExp.
    def enterBinCompExp(self, ctx:SkillsParser.BinCompExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#BinCompExp.
    def exitBinCompExp(self, ctx:SkillsParser.BinCompExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#BoolValue.
    def enterBoolValue(self, ctx:SkillsParser.BoolValueContext):
        pass

    # Exit a parse tree produced by SkillsParser#BoolValue.
    def exitBoolValue(self, ctx:SkillsParser.BoolValueContext):
        pass


    # Enter a parse tree produced by SkillsParser#Infinite.
    def enterInfinite(self, ctx:SkillsParser.InfiniteContext):
        pass

    # Exit a parse tree produced by SkillsParser#Infinite.
    def exitInfinite(self, ctx:SkillsParser.InfiniteContext):
        pass


    # Enter a parse tree produced by SkillsParser#ImpliesExp.
    def enterImpliesExp(self, ctx:SkillsParser.ImpliesExpContext):
        pass

    # Exit a parse tree produced by SkillsParser#ImpliesExp.
    def exitImpliesExp(self, ctx:SkillsParser.ImpliesExpContext):
        pass


    # Enter a parse tree produced by SkillsParser#statement.
    def enterStatement(self, ctx:SkillsParser.StatementContext):
        pass

    # Exit a parse tree produced by SkillsParser#statement.
    def exitStatement(self, ctx:SkillsParser.StatementContext):
        pass



del SkillsParser