import antlr4
import logging

class RobotLangException(Exception):
    pass


class RobotLangErrorListener(antlr4.error.ErrorListener.ErrorListener):
    def __init__(self):
        super(RobotLangErrorListener, self).__init__()

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        logging.getLogger('robot_lang').error("line %d:%d %s", line, column, msg)
        raise RobotLangException()

    def reportAmbiguity(self, recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs):
        logging.getLogger('robot_lang').error("ambiguity %s %s %s %s %s %s",
            dfa, startIndex, stopIndex, exact, ambigAlts, configs)
        raise RobotLangException()

    def reportAttemptingFullContext(self, recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs):
        logging.getLogger('robot_lang').error("attempting full context %s %s %s %s %s",
            dfa, startIndex, stopIndex, conflictingAlts, configs)
        raise RobotLangException()

    def reportContextSensitivity(self, recognizer, dfa, startIndex, stopIndex, prediction, configs):
        logging.getLogger('robot_lang').error("context sensitivity %s %s %s %s %s",
            dfa, startIndex, stopIndex, prediction, configs)
        raise RobotLangException()

