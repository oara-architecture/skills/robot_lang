from abc import abstractmethod, ABC
import jinja2

from ..ast.model import Model


class DotGenerator(ABC):
    def __init__(self, model: Model):
        self.__model = model

    def write(self, filename: str):
        with open(filename, 'w') as f:
            f.write(self._to_dot())

    @property
    def model(self) -> Model:
        return self.__model

    @abstractmethod
    def _to_dot(self) -> str:
        pass


class ResourceDot(DotGenerator):
    template = jinja2.Template("""
        digraph {{ resource }} {
            __start [shape=point]
            __start -> {{ initial }}
            {% for arc in arcs %}
            {{ arc.src }} -> {{ arc.dst }} {% if arc.extern is sameas true %}[style=dashed]{% endif %}
            {% endfor %}
        }
    """)

    def _to_dot(self) -> str:
        return self.template.render(resource=self.model.name,
                                    arcs=self.model.statemachine.arcs,
                                    initial=self.model.statemachine.initial)


class SkillDot(DotGenerator):
    template = jinja2.Template("""
        digraph {{ skill }} {
            __start [shape=point]
            NV [shape=box]
            NR [shape=box]
            {% if invariants %}
            RI [shape=box]
            {% endif %}
            {% for result in results %}
            M_{{ result }} [shape=box]
            N_{{ result }} [shape=box]
            {% endfor %}

            __start -> S [label="start",fontname="times italic"]
            S -> CR
            S -> NV
            CR -> NR
            CR -> RG [label="{% if preconditions %}{{ preconditions | join('\n & ') }} \n / {% if preconditions.success is none %}-{% else %}{{ preconditions.success }}{% endif %}{% endif %}"]
            {% for inv in invariants %}
            RG -> RI [label="not {{ inv.name }} \n / {% if inv.effect is not none %}{{ inv.effect }}{% else %}-{% endif %}"]
            {% endfor %}
            RG -> IG [label="interrupt",fontname="times italic"]
            {% for result in results %}
            RG -> T_{{ result }}
            IG -> T_{{ result }}
            T_{{ result }} -> M_{{ result }} [label="{% if result.expression %}{{ result.expression }}{% endif %}
                {%- if result.expression and result.post %} & {% endif %}
                {%- if result.post %}{{ result.post }}{% endif %}
                {%- if not (result.expression or result.post) %}true{% endif %} \n / {% if result.effect is none %}-{% else %}{{ result.effect }}{% endif %}"]
            T_{{ result }} -> N_{{ result }}
            {% endfor %}
        }
    """)

    def _to_dot(self) -> str:
        return self.template.render(skill=self.model.name,
                                    results=self.model.results,
                                    preconditions=self.model.preconditions,
                                    invariants=self.model.invariants)
