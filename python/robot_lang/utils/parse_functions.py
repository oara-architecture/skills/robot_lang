import antlr4
from typing import Tuple, Iterable

from ..ast.model import Model
from .exceptions import RobotLangErrorListener
from ..parser.SkillsLexer import SkillsLexer
from ..parser.SkillsParser import SkillsParser
from ..visitor.visitor import Visitor


def parse_stream(stream):
    lexer = SkillsLexer(stream)
    token_stream = antlr4.CommonTokenStream(lexer)
    parser = SkillsParser(token_stream)
    parser.removeErrorListeners()
    parser.addErrorListener(RobotLangErrorListener())
    tree = parser.model()
    v = Visitor(stream)
    model = v.visitModel(tree)
    return tree, model


def parse_file(file: str) -> Tuple[SkillsParser.ModelContext, Model]:
    """Parse a file.
    
        :param file: file name
        :return: parsed tree and model
    """
    input_stream = antlr4.FileStream(file)
    return parse_stream(input_stream)


def parse_string(s: str) -> Tuple[SkillsParser.ModelContext, Model]:
    """Parse a string.
    
        :param s: string
        :return: parsed tree and model
    """
    input_stream = antlr4.InputStream(s)
    return parse_stream(input_stream)


def parse_all(files: Iterable[str]) -> Model:
    """Parse a set of files.
    
        :param files: filenames
        :return: parsed model
    """
    model = Model()
    for file in files:
        _, m = parse_file(file)
        model.extend(m)
    return model
