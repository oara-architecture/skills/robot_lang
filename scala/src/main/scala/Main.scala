import org.antlr.v4.runtime._
import org.antlr.v4.runtime.tree._
import parser._
import listener._
import factory.{Model => ModelFactory}
import model._

object Main extends App {
  println("-" * 100)
  val file = "files/test.skill"
  val input = (new ANTLRFileStream(file))
  val lexer = new SkillsLexer(input)
  val tokens = new CommonTokenStream(lexer)
  val parser = new SkillsParser(tokens)
  val tree = parser.model()

  val modelFactory = new ModelFactory
  val result = new Result

  println("-" * 100);
  { //------------------------- Type -------------------------
    val listener = new TypeListener(modelFactory, file, result)
    ParseTreeWalker.DEFAULT.walk(listener, tree)
    println(">>> Type Definition <<<")
  }
  { //------------------------- Robot -------------------------
    val listener = new RobotListener(modelFactory, file, result)
    ParseTreeWalker.DEFAULT.walk(listener, tree)
    println(">>> Robot Definition <<<")
  }
  { //------------------------- Skill -------------------------
    val listener = new SkillListener(modelFactory, file, result)
    ParseTreeWalker.DEFAULT.walk(listener, tree)
    println(">>> Skill Definition <<<")
  }
  if (!result.isOk) {
    println("-" * 100)
    println(result)
  }
  println("-" * 100)

  val model = modelFactory.create
  println(model.pretty)

}
