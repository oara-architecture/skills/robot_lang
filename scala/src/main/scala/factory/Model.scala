package factory

import scala.collection.mutable.ArrayBuffer
import listener._

class Model {
  //------------------------- Type -------------------------

  private val type_buffer = ArrayBuffer.empty[Element[String]]

  def types = type_buffer.toList

  def type_add(t: Element[String]) = type_buffer += t

  def type_get(name: String): Option[Element[String]] =
    type_buffer.find(_.element == name)

  //------------------------- Robot -------------------------

  private val robot_buffer = ArrayBuffer.empty[Element[Robot]]

  def robots = robot_buffer.toList

  def robot_add(r: Element[Robot]) = robot_buffer += r

  def robot_get(name: String): Option[Element[Robot]] =
    robot_buffer.find(_.element.name == name)

  //------------------------- Create -------------------------

  def create = model.Model(types.map(_.element), robots.map(_.element.create))
}
