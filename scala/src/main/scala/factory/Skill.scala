package factory

import scala.collection.mutable.ArrayBuffer
import listener._
import model.{Input, Mode, Use}

class Skill(val name: String) {
  private var interruptible: Boolean = false

  def set_interruptible(b: Boolean) = interruptible = b

  //------------------------- Input -------------------------

  private val input_buffer = ArrayBuffer.empty[Element[Input]]

  def inputs = input_buffer.toList

  def input_add(s: Element[Input]) = input_buffer += s

  def input_get(name: String): Option[Element[Input]] =
    input_buffer.find(_.element.name == name)

  //------------------------- Mode -------------------------

  private val mode_buffer = ArrayBuffer.empty[Element[Mode]]

  def modes = mode_buffer.toList

  def mode_add(m: Element[Mode]) = mode_buffer += m

  def mode_get(name: String): Option[Element[Mode]] =
    mode_buffer.find(_.element.name == name)

  //------------------------- Use -------------------------

  private val use_buffer = ArrayBuffer.empty[Element[Use]]

  def uses = use_buffer.toList

  def use_add(m: Element[Use]) = use_buffer += m

  def use_get(name: String): Option[Element[Use]] =
    use_buffer.find(_.element.resource.name == name)

  //------------------------- Model -------------------------

  def create =
    model.Skill(
      name,
      interruptible,
      inputs.map(_.element),
      modes.map(_.element),
      uses.map(_.element)
    )

}
