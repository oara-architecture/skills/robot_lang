package factory

import scala.collection.mutable.ArrayBuffer
import listener._
import model.{Data, Parameter, Function, Resource}

class Robot(val name: String) {

  //------------------------- Data -------------------------

  private val data_buffer = ArrayBuffer.empty[Element[Data]]

  def data = data_buffer.toList

  def data_add(d: Element[Data]) = data_buffer += d

  def data_get(name: String): Option[Element[Data]] =
    data_buffer.find(_.element.name == name)

  //------------------------- Function -------------------------

  private val fun_buffer = ArrayBuffer.empty[Element[Function]]

  def functions = fun_buffer.toList

  def function_add(f: Element[Function]) = fun_buffer += f

  def function_get(name: String): Option[Element[Function]] =
    fun_buffer.find(_.element.name == name)

  //------------------------- Resource -------------------------

  private val res_buffer = ArrayBuffer.empty[Element[Resource]]

  def resources = res_buffer.toList

  def resource_add(r: Element[Resource]) = res_buffer += r

  def resource_get(name: String): Option[Element[Resource]] =
    res_buffer.find(_.element.name == name)

  //------------------------- Skill -------------------------

  private val skill_buffer = ArrayBuffer.empty[Element[Skill]]

  def skills = skill_buffer.toList

  def skill_add(s: Element[Skill]) = skill_buffer += s

  def skill_get(name: String): Option[Element[Skill]] =
    skill_buffer.find(_.element.name == name)

  //------------------------- Model -------------------------

  def create =
    model.Robot(
      name,
      data.map(_.element),
      functions.map(_.element),
      resources.map(_.element),
      skills.map(_.element.create)
    )
}
