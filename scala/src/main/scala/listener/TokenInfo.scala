package listener

case class TokenInfo(file: String, line: Int, column: Int) {
  override def toString: String = file + ':' + line + ':' + column
}

case class Element[T](element: T, info: TokenInfo)
