package listener

import collection.JavaConverters._

import parser._
import factory._
import model.{Resource, Input, Mode, SuccessMode, FailureMode, Use}
import scala.collection.mutable.ArrayBuffer

class SkillListener(
    val model: Model,
    val file: String,
    val result: Result
) extends Listener {

  private var robot: Robot = null
  private var skill: Element[Skill] = null

  override def enterRobot(ctx: SkillsParser.RobotContext) = {
    val info = token_info(ctx.start)
    val name = ctx.name.getText
    model.robot_get(name) match {
      case Some(r) => robot = r.element
      case None =>
        result += Error(
          info,
          "robot \"" + name + "\" is undefined"
        )
        robot = null
    }
  }

  override def enterSkill(ctx: SkillsParser.SkillContext) = {
    if (robot != null) {
      val info = token_info(ctx.start)
      val name = ctx.name.getText

      robot.skill_get(name) match {
        case Some(s) =>
          result += Error(
            info,
            "skill \"" + name + "\" already defined at " + s.info
          )
          skill = null
        case None =>
          skill = Element(new Skill(name), info)
          robot.skill_add(skill)
      }
    }
  }

  override def enterInterruptible(ctx: SkillsParser.InterruptibleContext) = {
    if (skill != null) {
      skill.element.set_interruptible(true)
    }
  }

  override def enterInputDef(ctx: SkillsParser.InputDefContext) = {
    if (skill != null) {
      val info = token_info(ctx.start)
      val name = ctx.name.getText
      // check input
      skill.element.input_get(name) match {
        case Some(i) =>
          result += Error(
            info,
            "input \"" + name + "\" already defined at " + i.info
          )
        case None =>
          check_type(ctx.inputType) match {
            case None =>
            case Some(inputType) =>
              val input = Input(name, inputType)
              skill.element.input_add(Element(input, info))
          }
      }
    }
  }

  override def enterSuccessMode(ctx: SkillsParser.SuccessModeContext) = {
    if (skill != null) {
      val info = token_info(ctx.start)
      val name = ctx.name.getText
      // check mode
      skill.element.mode_get(name) match {
        case Some(m) =>
          result += Error(
            info,
            "mode \"" + name + "\" already defined at " + m.info
          )
        case None =>
          val probability = ctx.probability.getText.toDouble
          val mode = SuccessMode(name, probability)
          skill.element.mode_add(Element(mode, info))
      }
    }
  }

  override def enterFailureMode(ctx: SkillsParser.FailureModeContext) = {
    if (skill != null) {
      val info = token_info(ctx.start)
      val name = ctx.name.getText
      // check mode
      skill.element.mode_get(name) match {
        case Some(m) =>
          result += Error(
            info,
            "mode \"" + name + "\" already defined at " + m.info
          )
        case None =>
          val mode = FailureMode(name)
          skill.element.mode_add(Element(mode, info))
      }
    }
  }

  override def enterUseDef(ctx: SkillsParser.UseDefContext) = {
    if (skill != null) {
      val info = token_info(ctx.start)
      val name = ctx.name.getText
      // check resource
      robot.resource_get(name) match {
        case None =>
          result += Error(
            info,
            "resource \"" + name + "\" is undefined"
          )
        case Some(resource) =>
          // begin
          val begin = if (ctx.begin != null) {
            val n = ctx.begin.getText
            if (resource.element.states.contains(n))
              Some(n)
            else {
              result += Error(
                token_info(ctx.begin),
                "state \"" + n + "\" is undefined"
              )
              None
            }
          } else None
          // during
          val during = if (ctx.during != null) {
            val n = ctx.during.getText
            if (resource.element.states.contains(n))
              Some(n)
            else {
              result += Error(
                token_info(ctx.during),
                "state \"" + n + "\" is undefined"
              )
              None
            }
          } else None
          // end
          val end = if (ctx.end != null) {
            val n = ctx.end.getText
            if (resource.element.states.contains(n))
              Some(n)
            else {
              result += Error(
                token_info(ctx.end),
                "state \"" + n + "\" is undefined"
              )
              None
            }
          } else None
          val use = Use(resource.element, begin, during, end)
          skill.element.use_add(Element(use, info))
      }
    }
  }

  override def enterPrecondition(ctx: SkillsParser.PreconditionContext) = {
    val v = new visitor.ExpressionVisitor
    val exp = v.visit(ctx.expr)
    println("=> exp: " + exp)
  }
}
