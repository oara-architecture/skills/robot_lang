package listener

import parser._
import factory._

trait Listener extends SkillsBaseListener {

  def model: Model
  def file: String
  def result: Result

  def token_info(token: org.antlr.v4.runtime.Token) =
    TokenInfo(file, token.getLine, token.getCharPositionInLine)

  def check_type(token: org.antlr.v4.runtime.Token): Option[String] = {
    val name = token.getText
    model.type_get(name) match {
      case None =>
        result += Error(
          token_info(token),
          "undefined type \"" + name + "\""
        )
        None
      case Some(_) => Some(name)
    }
  }
}
