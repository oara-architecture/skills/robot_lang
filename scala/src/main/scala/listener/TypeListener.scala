package listener

import parser._
import factory._
import scala.collection.mutable.ArrayBuffer

class TypeListener(
    val model: Model,
    val file: String,
    val result: Result
) extends Listener {

  override def enterTypeDef(ctx: SkillsParser.TypeDefContext) = {
    val info = token_info(ctx.start)
    val name = ctx.name.getText
    model.type_get(name) match {
      case Some(e) =>
        result += Error(
          info,
          "type \"" + name + "\" already defined at " + e.info
        )
      case None => model.type_add(Element(name, info))
    }
  }
}
