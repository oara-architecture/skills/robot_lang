package listener

import scala.collection.mutable

case class Error(info: TokenInfo, message: String) {
  override def toString: String = info.toString + ": " + message
}

class Result {
  private val buffer = mutable.ArrayBuffer.empty[Error]

  def isOk: Boolean = buffer.isEmpty
  def isError: Boolean = buffer.nonEmpty

  def errors: List[Error] = buffer.toList

  def +=(error: Error) = buffer += error

  override def toString: String = ("" /: errors)(_ + "\n" + _)
}
