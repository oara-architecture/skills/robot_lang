package listener

import collection.JavaConverters._

import parser._
import factory._
import model.{Data, Parameter, Function, Resource, Arc}
import scala.collection.mutable.ArrayBuffer

class RobotListener(
    val model: Model,
    val file: String,
    val result: Result
) extends Listener {

  private var robot: Element[Robot] = null
  private var resource: Element[Resource] = null

  override def enterRobot(ctx: SkillsParser.RobotContext) = {
    val info = token_info(ctx.start)
    val name = ctx.name.getText
    model.robot_get(name) match {
      case Some(e) =>
        result += Error(
          info,
          "robot \"" + name + "\" already defined at " + e.info
        )
        robot = null
      case None =>
        robot = Element(new Robot(name), info)
        model.robot_add(robot)
    }
  }

  override def enterDataDef(ctx: SkillsParser.DataDefContext) = {
    if (robot != null) {
      val info = token_info(ctx.start)
      val name = ctx.name.getText
      // check data
      robot.element.data_get(name) match {
        case Some(d) =>
          result += Error(
            info,
            "data \"" + name + "\" already defined at " + d.info
          )
        case None =>
          check_type(ctx.dataType) match {
            case None =>
            case Some(dataType) =>
              val data = Data(name, dataType)
              robot.element.data_add(Element(data, info))
          }
      }
    }
  }

  override def enterFunctionDef(ctx: SkillsParser.FunctionDefContext) = {
    if (robot != null) {
      val info = token_info(ctx.start)
      val name = ctx.name.getText
      var ok = true
      // check function name
      robot.element.function_get(name) match {
        case Some(f) =>
          result += Error(
            info,
            "function \"" + name + "\" already defined at " + f.info
          )
          ok = false
        case None =>
      }
      // check parameters types
      val pNames = ctx.pNames.asScala.toList
      val pTypes = ctx.pTypes.asScala.toList
      val params = pNames.zip(pTypes)
      for ((n, t) <- params) {
        check_type(t) match {
          case None => ok = false
          case _    =>
        }
      }
      val parameters = params.map(x => Parameter(x._1.getText, x._2.getText))
      // check return type
      val retType =
        (
          if (ctx.retType != null) {
            check_type(ctx.retType) match {
              case None =>
                ok = false
                None
              case x => x
            }
          } else None
        )

      if (ok) {
        val fun = Function(name, parameters, retType)
        robot.element.function_add(Element(fun, info))
      }
    }
  }

  override def enterResource(ctx: SkillsParser.ResourceContext) = {
    val info = token_info(ctx.start)
    val name = ctx.name.getText
    // check function name
    robot.element.resource_get(name) match {
      case Some(r) =>
        result += Error(
          info,
          "resource \"" + name + "\" already defined at " + r.info
        )
        resource = null
      case None =>
        resource = Element(Resource(name, "", Nil), info)
    }
  }

  override def enterResourceInitial(ctx: SkillsParser.ResourceInitialContext) {
    if (resource != null) {
      val initial = ctx.initial.getText
      resource = Element(resource.element.set_initial(initial), resource.info)
    }
  }

  override def enterResourceArc(ctx: SkillsParser.ResourceArcContext) {
    if (resource != null) {
      val src = ctx.src.getText
      val dst = ctx.dst.getText
      val arc = Arc(src, dst)
      resource = Element(resource.element += arc, resource.info)
    }
  }

  override def exitResource(ctx: SkillsParser.ResourceContext) = {
    if (resource != null) {
      robot.element.resource_add(resource)
    }
  }
}
