package ast

sealed abstract class Expression

object BinCompOp extends Enumeration {
  val EQ = Value("==")
  val NE = Value("!=")
  val LE = Value("<=")
  val LT = Value("<")
  val GE = Value(">=")
  val GT = Value(">")

}

object BinBoolOp extends Enumeration {
  val AND = Value("&&")
  val OR = Value("||")
  val IMPLIES = Value("=>")
}

final case class BinComp(
    left: Expression,
    operator: BinCompOp.Value,
    right: Expression
) extends Expression {
  override def toString: String = s"""(${left} ${operator} ${right})"""
}

final case class BinBool(
    left: Expression,
    operator: BinBoolOp.Value,
    right: Expression
) extends Expression {
  import BinCompOp._
  override def toString: String = s"""(${left} ${operator} ${right})"""
}

final case class Identifier(name: String) extends Expression {
  override def toString: String = name
}
