package visitor

import parser.SkillsBaseVisitor
import ast._
import parser._
import parser.SkillsParser._

class ExpressionVisitor extends SkillsBaseVisitor[Expression] {

  override def visitBinCompExp(ctx: BinCompExpContext): Expression = {
    val left = this.visit(ctx.left)
    val right = this.visit(ctx.right)
    val op = ctx.op.getType match {
      case SkillsParser.EQ => BinCompOp.EQ
      case SkillsParser.NE => BinCompOp.NE
      case SkillsParser.LT => BinCompOp.LT
      case SkillsParser.LE => BinCompOp.LE
      case SkillsParser.GT => BinCompOp.GT
      case SkillsParser.GE => BinCompOp.GE
    }
    BinComp(left, op, right)
  }

  override def visitBinBoolExp(ctx: BinBoolExpContext): Expression = {
    val left = this.visit(ctx.left)
    val right = this.visit(ctx.right)
    val op = ctx.op.getType match {
      case SkillsParser.AND     => BinBoolOp.AND
      case SkillsParser.OR      => BinBoolOp.OR
      case SkillsParser.IMPLIES => BinBoolOp.IMPLIES
    }
    BinBool(left, op, right)
  }

  override def visitIdentifierExp(ctx: IdentifierExpContext): Expression = {
    Identifier(ctx.getText)
  }
}
