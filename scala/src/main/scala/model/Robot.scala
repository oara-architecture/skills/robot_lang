package model

//========================= Data =========================

case class Data(name: String, dataType: String) {
  override def toString: String = name
  def pretty: String = name + ": " + dataType
}

//========================= Parameter =========================

case class Parameter(name: String, paramType: String) {
  override def toString: String = name
  def pretty: String = name + ": " + paramType
}

//========================= Function =========================

case class Function(
    name: String,
    parameters: List[Parameter],
    retType: Option[String]
) {
  override def toString: String = name
  def pretty: String = {
    var s = name + "("
    if (parameters.nonEmpty) {
      s += (parameters.head.toString /: parameters.tail)(_ + ", " + _.toString)
    }
    s += ")"
    retType match {
      case Some(t) => s += ": " + t
      case None    =>
    }
    return s
  }
}

//========================= Arc =========================

case class Arc(src: String, dst: String)

//========================= Resource =========================

case class Resource(
    name: String,
    initial: String,
    transitions: List[Arc]
) {

  def set_initial(state: String): Resource = Resource(name, state, transitions)

  def +=(arc: Arc): Resource = Resource(name, initial, transitions ++ List(arc))

  def states: List[String] =
    transitions.flatMap(a => List(a.src, a.dst)).distinct

  override def toString = name
  def pretty: String = {
    val sb = new StringBuilder
    sb ++= "\tresource " + name + ": statemachine {\n"
    sb ++= "\t\tinitial " + initial + "\n"
    for (a <- transitions) {
      sb ++= "\t\t" + a.src + " -> " + a.dst + "\n"
    }
    sb ++= "\t}\n"
    sb.toString
  }
}

//========================= Robot =========================

case class Robot(
    name: String,
    data: List[Data],
    functions: List[Function],
    resources: List[Resource],
    skills: List[Skill]
) {

  override def toString: String = name

  def pretty: String = {
    val sb = new StringBuilder
    sb ++= "robot " + name + " {\n"
    // Data
    sb ++= "\tdata {\n"
    for (d <- data) {
      sb ++= "\t\t" + d.pretty + "\n"
    }
    sb ++= "\t}\n"
    // Funciton
    sb ++= "\tfunction {\n"
    for (f <- functions) {
      sb ++= "\t\t" + f.pretty + "\n"
    }
    sb ++= "\t}\n"
    // Resource
    for (r <- resources) {
      sb ++= r.pretty
    }
    // Skill
    for (s <- skills) {
      sb ++= s.pretty
    }
    // ---
    sb ++= "}\n"
    sb.toString
  }
}
