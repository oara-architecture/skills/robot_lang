package model

case class Model(types: List[String], robots: List[Robot]) {

  def pretty: String = {
    val sb = new StringBuilder
    // Types
    for (t <- types) {
      sb ++= "type " + t + '\n'
    }
    // Robots
    for (r <- robots) {
      sb ++= r.pretty
    }
    sb.toString
  }
}
