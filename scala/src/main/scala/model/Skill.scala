package model

//========================= Input =========================

case class Input(name: String, inputType: String) {
  override def toString: String = name
  def pretty: String = name + ": " + inputType
}

//========================= Mode =========================

sealed abstract class Mode {
  def name: String
  override def toString: String = name
  def pretty: String = name
}
case class SuccessMode(name: String, probability: Double) extends Mode {
  override def pretty: String =
    "success " + name + " probability=" + probability
}
case class FailureMode(name: String) extends Mode {
  override def pretty: String = "failure " + name
}

//========================= Use =========================

case class Use(
    resource: Resource,
    begin: Option[String],
    during: Option[String],
    end: Option[String]
) {
  override def toString: String = resource.name
  def pretty: String = {
    var str = resource.name
    if (begin.isDefined) str += " begin=" + begin.get
    if (during.isDefined) str += " during=" + during.get
    if (end.isDefined) str += " end=" + end.get
    str
  }
}

//========================= Skill =========================

case class Skill(
    name: String,
    interruptible: Boolean,
    input: List[Input],
    mode: List[Mode],
    use: List[Use]
) {

  override def toString: String = name

  def pretty: String = {
    val sb = new StringBuilder
    sb ++= "\tskill " + name + " {\n"
    // Interruptible
    if (interruptible) sb ++= "\t\tinterruptible\n"
    // Input
    sb ++= "\t\tinput {\n"
    for (i <- input)
      sb ++= "\t\t\t" + i.pretty + "\n"
    sb ++= "\t\t}\n"
    // Mode
    sb ++= "\t\tmode {\n"
    for (m <- mode)
      sb ++= "\t\t\t" + m.pretty + "\n"
    sb ++= "\t\t}\n"
    // Use
    sb ++= "\t\tuse {\n"
    for (u <- use)
      sb ++= "\t\t\t" + u.pretty + "\n"
    sb ++= "\t\t}\n"
    //
    sb ++= "\t}\n"
    sb.toString
  }
}
