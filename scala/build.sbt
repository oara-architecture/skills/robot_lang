import sbt._
import scala.sys.process._

//--------------------------------------------------

val scala_version = "2.12.10"
val project_name = "robot_model"
val project_version = "0.1.1"

// jfxSettings

// JFX.mainClass := Some("mypackage.MyJavaFXApplication")

scalaVersion := scala_version
version := project_version
name := project_name

//--------------------------------------------------

fork in run := true
javaOptions += "-Xmx8G"
javaOptions += "-Xss100M"

javacOptions += "-Xlint:unchecked"
javacOptions += "-Xdiags:verbose"

// Dotty
// scalacOptions ++= { if (isDotty.value) Seq("-language:Scala2") else Nil }

val osName: SettingKey[String] = SettingKey[String]("osName")

osName := (System.getProperty("os.name") match {
  case name if name.startsWith("Linux")   => "linux"
  case name if name.startsWith("Mac")     => "mac"
  case name if name.startsWith("Windows") => "win"
  case _                                  => throw new Exception("Unknown platform!")
})
// osName := "win"

//--------------------------------------------------

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x                             => MergeStrategy.first
}

organization := "Onera"
libraryDependencies += "org.scala-lang" % "scala-library" % scalaVersion.value
// libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
// libraryDependencies += "org.scala-lang" % "scala-compiler" % scalaVersion.value

libraryDependencies += "org.antlr" % "antlr4-runtime" % "4.8"
// "org.antlr" % "stringtemplate" % "3.2"

//--------------------------------------------------

/*
def release_lib = Command.command("release_lib") { state =>
  val cmd = s"""cp -f target/scala-2.12/${project_name}_2.12-${project_version}.jar release/${project_name}_lib.jar"""
  println(cmd)
  val x = cmd.!
  state
}

def release = Command.command("release") { state =>
  val cmd = s"""cp -f target/scala-2.12/${project_name}-assembly-${project_version}.jar release/${project_name}.jar"""
  println(cmd)
  val x = cmd.!
  state
}

commands ++= Seq(release_lib, release)
 */

//--------------------------------------------------
