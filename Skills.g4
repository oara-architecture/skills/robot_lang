grammar Skills;

// TODO: change le nom du langage TODO: ajouter robot block

/** Root rule of the grammar */
model: (types += typeBlock | skillsets += skillset)*;

//========================= Type =========================

/** A block of types. */
typeBlock:
	TYPE LBRACE (types += typeDef)* RBRACE
	| TYPE types += typeDef;

/** A type is declared by giving a name. */
typeDef: name = IDENTIFIER;

//========================= SkillSet =========================

/** A skillset is made of data, functions, resources and skills. */
skillset:
	SKILLSET name = IDENTIFIER LBRACE (
		data += dataBlock
		| functions += functionBlock
		| resources += resourceBlock
		| skills += skillBlock
	)* RBRACE;

//---------- Data ---------- 

/** A block of data. */
dataBlock:
	DATA LBRACE (data += dataDef)* RBRACE
	| DATA data += dataDef;

/** A data is declared by a name and a type */
dataDef: name = IDENTIFIER COLON dataType = IDENTIFIER;

//---------- Function ---------- 

/** A block of functions. */
functionBlock:
	FUNCTION LBRACE (functions += functionDef)* RBRACE
	| FUNCTION functions += functionDef;

/** A function is declared by its signature. */
functionDef:
	name = IDENTIFIER LPAREN (
		params += parameterDef (COMMA params += parameterDef)*
	)? RPAREN (COLON funType = IDENTIFIER)?;

/** A parameter has a name and a type. */
parameterDef: name = IDENTIFIER COLON paramType = IDENTIFIER;

//---------- Resource ---------- 

/** A block of resources. */
resourceBlock:
	RESOURCE LBRACE (resources += resourceDef)* RBRACE
	| RESOURCE resources += resourceDef;

/** A resource has a name, and contains an initial state and ars. */
resourceDef:
	name = IDENTIFIER LBRACE initial = resourceInitial (
		arcs += resourceArc
	)* RBRACE;

/** Resource initial state. */
resourceInitial: INITIAL name = IDENTIFIER;

/** A Resource arc. */
resourceArc:
	(is_extern = EXTERN)? src = IDENTIFIER ARROW dst = IDENTIFIER;

//========================= Skill =========================

/**  A block of skills. */
skillBlock:
	SKILL LBRACE (skills += skillDef)* RBRACE
	| SKILL skills += skillDef;

/** A skill definition. */
skillDef:
	name = IDENTIFIER LBRACE //
	PROGRESS ASSIGN progress = (INTEGER | FLOATING) //
	(inputs = inputBlock)? //
	(effects = effectBlock)? //
	(preconditions = preconditionBlock)? //
	(invariants = invariantBlock)? //
	(INTERRUPTION EFFECT? interrupt_effect = IDENTIFIER)? //
	(results = resultBlock)? //
	RBRACE;

//---------- Input ---------- 

/** A block of inputs. */
inputBlock:
	(INPUT LBRACE (inputs += inputDef)* RBRACE)
	| (INPUT inputs += inputDef);

/** An input is defined by a name and a type. */
inputDef: name = IDENTIFIER COLON inputType = IDENTIFIER;

//---------- Effect ---------- 

/** A block of effects. */
effectBlock:
	EFFECT LBRACE (effects += effectDef)* RBRACE
	| EFFECT effects += effectDef;

/** An effect consits in a set of effects on resources. */
effectDef:
	(name = IDENTIFIER COLON (effects += resourceEffect)+)
	| (
		name = IDENTIFIER LBRACE //
		(effects += resourceEffect)* //
		RBRACE
	);

/** An effect on a resource consists in changing the resource state.  */
resourceEffect: name = IDENTIFIER ARROW state = IDENTIFIER;

//---------- Precondition ---------- 

/** A block of preconditions. */
preconditionBlock:
	(
		PRECONDITION LBRACE //
		(preconds += preconditionDef)* //
		(SUCCESS success = IDENTIFIER)? //
		RBRACE
	)
	| (
		PRECONDITION preconds += preconditionDef //
		(SUCCESS ASSIGN success = IDENTIFIER)?
	);

/** A precondition is identified by a name, and can contain a logic expression, a resource expression, and an effect in case of failure */
preconditionDef:
	(
		name = IDENTIFIER COLON //
		(expr = expression)? //
		(RESOURCE ASSIGN res_expr = resourceExpression)? //
		(FAILURE ASSIGN failure = IDENTIFIER)?
	)
	| (
		name = IDENTIFIER LBRACE //
		(expr = expression)? //
		(RESOURCE res_expr = resourceExpression)? //
		(FAILURE failure = IDENTIFIER)? //
		RBRACE
	);

//---------- Invariants ---------- 

/** A block of invariants. */
invariantBlock:
	(INVARIANT LBRACE (invariants += invariantDef)* RBRACE)
	| (INVARIANT invariants += invariantDef);

/** An invariant is defined by a name, and expression (both logic and on resources) and an effect in case of violation. */
invariantDef:
	(
		name = IDENTIFIER COLON //
		(expr = expression)? //
		(RESOURCE ASSIGN res_expr = resourceExpression)? //
		(VIOLATION ASSIGN effect = IDENTIFIER)?
	)
	| (
		name = IDENTIFIER LBRACE //
		(expr = expression)? //
		(RESOURCE res_expr = resourceExpression)? //
		(VIOLATION effect = IDENTIFIER)? //
		RBRACE
	);

//---------- Result ---------- 

/** A block of results. */
resultBlock:
	(RESULT LBRACE (results += resultDef)* RBRACE)
	| (RESULT results += resultDef);

/** A terminal result is defined by a name, an expression (both logic and on resources) and an
 effect to apply. An expression representing the expected duration until completion can also be
 defined.
 */
resultDef:
	(
		name = IDENTIFIER LBRACE //
		(expr = expression)? //
		(DURATION duration = expression)? //
		(POST post = resourceExpression)? //
		(APPLY effects += IDENTIFIER+)? //
		RBRACE
	)
	| (
		name = IDENTIFIER COLON //
		(expr = expression)? //
		(DURATION ASSIGN duration = expression)? //
		(POST ASSIGN post = resourceExpression)? //
		(APPLY ASSIGN effects += IDENTIFIER+)? //
	);

//---------- expression ---------- 

/** An expression on resources. It can be made of and, or, and implication operators, and test resource state. */
resourceExpression:
	name = IDENTIFIER op = (EQ | NE) state = IDENTIFIER									# BinCompResExp
	| left = resourceExpression op = (AND | OR | IMPLIES) right = resourceExpression	# BinBoolResExp
	| LPAREN expr = resourceExpression RPAREN											# ParenResExp;

/** A logic expression. The expression propositions are functions, data, and inputs. */
expression:
	left = expression op = (GT | GE | LT | LE | EQ | NE) right = expression	# BinCompExp
	| op = NOT expr = expression											# NotExp
	| left = expression op = AND right = expression							# AndExp
	| left = expression op = OR right = expression							# OrExp
	| left = expression op = IMPLIES right = expression						# ImpliesExp
	| LPAREN expr = expression RPAREN										# ParenExp
	| function = IDENTIFIER //
	LPAREN (params += expression (COMMA params += expression)*)? RPAREN	# FunCall
	| value = (TRUE | FALSE)											# BoolValue
	| value = (INTEGER | FLOATING)										# Number
	| INF																# Infinite
	| name = IDENTIFIER													# Identifier;
// FunctionCall

//---------- statement ---------- 

statement: IDENTIFIER ASSIGN expression SEMI;

//========================= Lexer =========================

//---------- Keywords ---------- 
TYPE: 'type';
SKILLSET: 'skillset';
DATA: 'data';
FUNCTION: 'function';
INITIAL: 'initial';
ARROW: '->';
// TODO: change this part
RESOURCE: 'resource';
EXTERN: 'extern';
SKILL: 'skill';
INPUT: 'input';
PRECONDITION: 'precondition';
RESULT: 'result';
DURATION: 'duration';
POST: 'post';
EFFECT: 'effect';
SUCCESS: 'success';
FAILURE: 'failure';
INVARIANT: 'invariant';
VIOLATION: 'violation';
INTERRUPTION: 'interruption';
APPLY: 'apply';
// USE: 'use'; PRE: 'pre'; START: 'start'; INV: 'inv';
PROGRESS: 'progress';
// POSTCONDITION: 'postcondition';
INF: 'inf';

TRUE: 'true';
FALSE: 'false';

//---------- Seprators ---------- 

LPAREN: '(';
RPAREN: ')';
LBRACE: '{';
RBRACE: '}';
LBRACK: '[';
RBRACK: ']';

SEMI: ';';
COMMA: ',';
DOT: '.';
COLON: ':';

//---------- Operators ---------- 

GT: '>';
GE: '>=';
LT: '<';
LE: '<=';
EQ: '==';
NE: '!=';

NOT: '!';
AND: '&&';
OR: '||';
IMPLIES: '=>';

ASSIGN: '=';

NOT_USE: 'not';
OR_USE: 'or';

//---------- Fragments ---------- 

fragment Digit: [0-9];
fragment Sign: '+' | '-';
fragment DigitSequence: Digit+;

//---------- Integer ---------- 

/** A signed integer */
INTEGER: Sign? Digit+;

//---------- Floating ---------- 

/** A floating number. */
FLOATING:
	FractionalConstant ExponentPart?
	| DigitSequence ExponentPart;

fragment FractionalConstant:
	DigitSequence? '.' DigitSequence
	| DigitSequence '.';

fragment ExponentPart:
	'e' Sign? DigitSequence
	| 'E' Sign? DigitSequence;

//---------- Identifier ---------- 

/** An identifier is an alphanumeric string */
IDENTIFIER: [a-zA-Z][a-zA-Z_0-9]*;

//----------  ---------- 

// Whitespace and comments
WS: [ \t\r\n\u000C]+ -> skip;

COMMENT: '/*' .*? '*/' -> channel(HIDDEN);

LINE_COMMENT: '//' ~[\r\n]* -> channel(HIDDEN);
