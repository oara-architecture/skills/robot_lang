        effect reset res1->S1 res2->S3
        effect eff1 {
            res1 -> S2
            res4 -> S3
        }
        effect eff2 {
            res1 -> S2
            res4 -> S3
        }

        invariant toto (res1=S1 && res2=S3) violation=eff1
        invariant titi (res1=S1 && res2=S3) reset

        mode m2 {
            duration = f(input, data)
            post     = (res=S2)
            apply    = reset
            // 0/ lock resources
            // 1/ Post
            // 2/ Check Res Effect
            // 3/ Hook Effect 
            //   -> no computation / no lock
            //   -> set resources => false
            // 4/ Res Effect + unlock
        }